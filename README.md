Site internet CPCE
==================

## Structure du dépôt

### `forms` : formulaires ChronoForms
Pour ses pages personnalisées le site utilise le composant ChronoForms qui permet, à la base, de concevoir des formulaires. Il est utilisé principalement de manière détournée en se servant quasi exclusivement de la fonctionnalité de code personnalisé ([voir la section dédiée](#chronoforms)). Le dossien `forms` contient le code de ces « formulaires ».

### `www` : structure du site (FTP)
Dans ce dossier sont regroupés les éléments personnalisés se trouvant sur le serveur hébergeant le site.

-   `images`: contient les images utilisées sur le site (logo, …)
-   `maintenance`: scripts de test pour diagnostiques des éventuels problèmes du site
-   `cpce`: tout le contenu spécifique au club (code, photos envoyées par les membres, documents, …)
    -   `clean-cache.sh`: script appelé périodiquement (cron, configurable depuis l'interface d'administration OVH) pour supprimer les miniatures générées lors de l'affichage des images pendant les votes et les concours
    -   `libcpce.php`: fonctions et variables mise en commun, utilisés par différents scripts
    -   `resize.php`: script de génération des miniatures
    -   `read_exif.php`: script d'extraction des donnée EXIF d'une image
    -   `joomla.inc.php`: fichier d'exemple des éléments à inclure pour accéder aux objets joomla à partir d'une page nue

Par ailleurs sur le site lui-même peuvent se trouver des éléments supplémentaires dans le dossier `www/cpce` :
-   `upload`: les photos envoyées par les membre pour les projections et concours _à venir_
-   `upload/default`: les photos envoyées par les membre pour leur galerie personnelle
-   `upload_archives`: une fois le concours ou la projection passés, déplacer le dossier correspondant ici
-   `*.zip`: fichiers gérérés lors de la récupération d'image, peuvent être supprimés sans vérification
-   `vote_is_open` et `hold_results`: fichiers utilisés pour la gestion des votes et publication des résultats. Ne sont pas nécessairement présents dans le répertoire. **Ne pas créer ou supprimer manuellement !**

### `concours` : documents et outils
Tous les fichiers et outils en rapport avec les concours internes du club :
-   `contest_post.tpl.html`: le modèle de post pour la publication des résultats d'un concours en page d'accueil, à copier et adapter pour chaque concours ;
-   `Règlement concours interne.dotx`: Modèle Word pour la génération du règlement d'un concours. En ouvrant le modèle il n'y a que quatre champs à remplir, trois sont dans le titre (MOIS, ANNÉE et THÈME DU CONCOURS) et un se trouve à l'article 7 en bas de la première page (DATE, affiché comme 00). Lors de la générations du PDF ces champs sont utilisés pour calculer les autres valeurs dans le document. Cela ne dispense pas de **relire** le PDF ainsi généré.
-   `concours.py` et `gen_sql.py`: scripts utilisée pour la préparation et le dépouillement des résultats. Leur utilisation est détaillée dans [le paragraphe dédié](#concours).

### Autre
`template_override.html`: le code de mise en forme du site permettant de corriger et améliorer certains aspects de l'interface. Depuis la partie d'administration du site, se configure en allant dans `Extensions > Templates`, `purity_III`, onglet `Custom Code` et en copiant/collant le contenu du fichier dans le champ `Before </head>`.

## Utilisation du site

### Gestion des inscriptions
La gestion des inscriptions et du paiement des cotisations s'effectue à partir de la page https://photo-club-ermont.fr/gestion-inscriptions.

Les nouveaux membres qui viennent de s'inscrire sur le site à l'adresse https://photo-club-ermont.fr/inscription apparaissent dans la liste « Inscrits en attente de validation ». Tout membre du bureau peut cliquer sur le bouton « Activer le compte » pour permettre au nouveau membre de se connecter au site. Une fois le compte activé le nouveau membre passe alors dans la liste « Membres » (le compte est ajouté au groupe « Membre »).

Pour chaque membre recensé le trésorier peut valider sa cotisation et/ou le paiement de son adhésion à la FPF en utilisant les boutons dédiés.

La désinscription se fait en cliquant sur le bouton ad-hoc. Cela a pour effet d'interdire la connexion au site (seule la partie publique reste accessible). Le compte du membre désinscrit est bloqué, et est déplacé dans le groupe « Non réinscrits ».

### Trombinoscope
La génération du trombinoscope est automatique. Il affiche tous les utilisateurs non bloqués qui ne font pas partie du groupe « Special » : c'est-à-dire tous les inscrits au club pour l'année courante.

Pour ajouter ou modifier la photo d'un membre sur le trombi, il faut se rendre dans [la partie administration du site](https://photo-club-ermont.fr/administrator/) et choisir le menu `Community Builder > Gestion des utilisateurs`. Trouver le membre concerné dans la liste (ou utiliser la recherche), cliquer sur son nom, sélectionner `Portrait` et le menu déroulant du champ `Selfie`. Pour valider le changement cliquer sur le bouton `Enregistrer et fermer`.

### Calendrier
Les événements se gèrent depuis la partie administration du site en sélectionnant dans le menu `Composants > !Cagenda > Évènements`. La page programme utilise les informations fournies ici pour afficher la liste des événements de l'année en cours.
-   La page n'affiche que les événements marqués comme « publiés ».
-   La date et l'heure de début/fin doit être renseignée dans la section « Évènement sur une période » de l'onglet `Dates`.
-   Pour les événements périodiques la permière occurence de l'événement (date et heure) doit être renseignée dans la section « Évènement sur une période », les occurences suivantes doivent être ajoutée dans la section « Dates uniques » en dessous.

### Téléchargement d'images
Pour permettre d'envoyer des images il suffit de se connecter au site en FTP et de créer un nouveau dossier dans `cpce/uploads`. Le nom de ce dossier doit respecter le format `AAAA.MM.JJ - Concours - Titre (N)`.

-   `AAAA.MM.JJ` est facultatif et représente la date, avec l'année sur quatre chiffres et le mois et le jour sur deux chiffres. Après cette date le thème n'est plus proposé aux membres dans la liste d'envoi des images.
-   « Concours » doit être précisé, après la date et avant le titre, dans le cas des… concours.
-   Le titre est celui du thème de la projection ou du concours, il n'y a pas de restriction particulière le concernant.
-   `(N)` est un nombre, inscrit entre parenthèses après le titre, il représente le nombre de photos que les membres peuvent proposer. Par défaut s'il n'est pas ajouté N=3.

Exemples :

-   2019.12.14 - Concours - L'empreinte du temps
-   2019.11.22 - Les sorties du CPCE (6)
-   Photo de vous enfant

La date permet de déterminer si l'entrée doit être présente où non dans la liste des photos à télécharger (les entrées dont la date est antérieure à la date courante sont masquées). Elle permet également de déterminer quelle série de photos afficher une fois le vote ouvert : c'est l'élément avec la date la plus ancienne (même si la date est passée, cela permet d'ouvrir les votes sur plusieurs jours), c'est pour cela qu'il faut penser à archiver les anciennes présentations.

**Note:** Penser à déplacer le dossier de la séance dans le dossier `upload_archives` **avant** l'ouverture du vote suivant pour éviter tout mélange de photo et confusion lors de la phase de vote.

### Vote
Le vote se passe à l'adresse https://photo-club-ermont.fr/vote.

Lorque le vote est fermé les adminitsrateurs ont deux boutons : l'un pour ouvrir le vote et permettre à tout le monde de voir et noter les photos, l'autre pour publier les résultats. Lorque le vote est ouvert les adminitsrateurs ont un bouton pour fermer le vote.

Le déroulement normal est donc :
-   Ouverture du vote par un administrateur (le fichier `vote_is_open` est créé) ;
-   Tout le monde met les notes et valide ses notes en cliquant sur le bouton « Envoyer » (chacun peut changer d'avis et modifier une ou plusieurs notes puis revalider en sur le bouton « Envoyer » autant de fois qu'il le souhaite) ;
-   Fermeture du vote par un administrateur (le fichier `vote_is_open` est supprimé et `hold_results` est créé) ;
-   Publications des résultats par un administrateur (le fichier `hold_results` est supprimé) ;
-   Visualisation du classement par tout le monde sur https://photo-club-ermont.fr/resultat-vote

### Concours
Le déroulement de l'organisation d'un concours comporte quatre étapes : la préparation (classement des image en ordre aléatoire ), le concours en lui même, pendant lequel les membres notent les photos, le dépouillement et la projection du résultat. Les paragraphes suivants détaillent chaque étape en différenciant, le cas échéant, le cas où les notes sont collectées sur papier (« mode manuel ») et le cas où le vote se fait par le site.

#### Préparation
Après la soumission des images par les membres, récupérer **par FTP** le dossier des images. Exécuter [le fichier `concours.py`](concours/concours.py) avec comme paramètre le dossier des images téléchargées. Celui-ci va créer :
-   un fichier `list.txt` contenant la liste des images originales, dans l'ordre aléatoire déterminé par le script ;
-   un répertoire `_liste` contenant toutes les images du concours et renommées selon le modèle : ID.jpg, l'ID étant le numéro attribué aléatoirement à l'image par le script (mode manuel) ;
-   un fichier `concours.csv` prêt à recevoir les notes pour le dépouillement (mode manuel).

Pour une utilisation du site pour la notation des images, copier le fichier `list.txt` généré dans le répertoire des images du concours.

#### Vote et dépouillement
##### Utilisation du site
Dans le cas où des note saisies manuellement sont à ajouter, les saisir manuellement dans un tableur et les enregistrer au format CSV dans un fichier nommé `notes.csv`. Exécuter la commande suivante, depuis le dossier où se trouve le fichier CSV :

    python3 gen_sql.py > notes.sql

Aller sur [la page de gestion de la base de données du site](https://phpmyadmin.cluster002.hosting.ovh.net/server_sql.php), coller le contenu du fichier `notes.sql` qui vient d'être créé dans le champ texte de la page et cliquer sur exécuter.

##### Mode manuel
Une fois le fichier CSV généré et les notes recueillies l'importer dans un tableur avec comme encodage UTF-8 et `;` comme séparateur.

1.   Copier/coller les notes après la dernière colonne. Les formules dans les colonnes total, moyenn, min, max et écart type se calculent automatiquemnt.
2.   Trier le tableau selon la colonne `Total` en ordre décroissant.
3.   Entrer `1` dans la cellule A2.
4.   Ajouter un `=` devant la formule de la cellule A3 et recopier la formule vers le bas.
5.   Enregistrer le fichier CSV et exécuter un nouvelle fois [le script `concours.py`](concours/concours.py) : les photos sont préfixées avec leur ordre d'arrivée au concours.

#### Projection
##### Utilisation du site
**Avant** de projeter sur l'écran, ouvrir [la page de résultat](https://photo-club-ermont.fr/resultat-vote), cliquer sur l'image arrivée première du classement et appuyer une fois sur la touche `←` pour afficher une image noire en plein écran.

Projeter sur l'écran, et faire défiler par appuis successifs sur la touche `←`, pour passer les photos de la dernière à la première.

##### Mode manuel
Lancer le programme habituel de diaporama dans le dossier en faisant attention de les projeter dans l'ordre alphabétique inverse.


## Chronoforms : détails techniques

Cette section détaille comment est utilisé le composant ChronoForms et comprendre l'utilisation des fichiers du dossier `forms`.

### Utilisation
Pour ses pages personnalisées le site utilise le composant ChronoForms qui permet, à la base, de concevoir des formulaires. Il est utilisé principalement de manière détournée en se servant quasi exclusivement de la fonctionnalité de code personnalisé.

Le code de chaque « formulaire » est accessible en cliquant sur le lien `Wizard edit` à côté du nom de celui-ci et en allant dans l'onglet `Events`. Là, plusieurs blocs de couleur verte indiquent les différent « événements » du formulaire, et pour chacun, un ou plusieurs blocs comme « Custom Code », « Load CSS » ou « Load JS » (servant, comme leur nom l'indique à exécuter du code PHP personnalisé, charger une feuille de style ou du code javascript).

L'événement par défaut est « Load » et correspond à l'affichage initial du formulaire. Les autres événements sont appelés en fonction de la valeur de la variable `$_GET['action']`.

Par exemple si le formulaire est accessible à l'adresse `/form_sample`, alors aller à l'URL `photo-club-ermont.fr/form_sample` déclenchera l'événement « Load » du formulaire et l'accès à `photo-club-ermont.fr/form_sample?action=save` déclenchera l'événement « Save ».

```
+-- on Load -------------------------------------------------------------------+
| +-- Custom Code -----------------------------------------------------------+ |
| |                                                                          | |
| +--------------------------------------------------------------------------+ |
| +-- Load CSS --------------------------------------------------------------+ |
| |                                                                          | |
| +--------------------------------------------------------------------------+ |
+------------------------------------------------------------------------------+

+-- on Save -------------------------------------------------------------------+
| +-- Custom Code -----------------------------------------------------------+ |
| |                                                                          | |
| +--------------------------------------------------------------------------+ |
| +-- Load CSS --------------------------------------------------------------+ |
| |                                                                          | |
| +--------------------------------------------------------------------------+ |
| +-- Load JS ---------------------------------------------------------------+ |
| |                                                                          | |
| +--------------------------------------------------------------------------+ |
+------------------------------------------------------------------------------+
```

### Convention de nommage
Le nom de chaque fichier du dossier `forms` est le même que celui du formulaire correspondant listé en allant dans `Composants > ChronoForms` depuis la partie administration du site.

Si le nom de fichier contient un tiret « - », le mot après ce tiret correspond à l'action du formulaire.

L'extension de chaque fichier correspond au type de bloc dans lequel il doit être copié/collé:
-   .php → Custom code
-   .css → Load CSS
-   .js → Load JS

### Astuces pour le codage (extraits de code)
Rien ne remplace la documentation officielle, mais voici quelques exemples simples d'utilisation.

#### Accès à la base de donnée
```php
$db = &JFactory::getDBO();
$table = $db->getPrefix().'table_name';

$db->setQuery('SELECT id, firstname, lastname FROM `$table`');
$result = $db->loadAssocList();
foreach ($result as $item) {
    echo $item['firstname'];
}

$db->setQuery('UPDATE `$table` SET id=0 WHERE lastname IS NULL');
$db->execute();
```
#### Objet utilisateur Joomla
```php
// get user by id
$user = &JFactory::getUser(173);
// get currently logged-in user (returns 0 if not logged in)
$current_user = &JFactory::getUser();
// get user groups
$groups = $user->getAuthorisedGroups()
```
#### Objet utilisateur CommunityBuilder
```php
include_once( JPATH_ADMINISTRATOR . '/components/com_comprofiler/plugin.foundation.php' );
$cbuser = CBuser::getUserDataInstance(173);

echo $cbuser->lastname;
$cbuser->set('cb_cotiz', 84);
```


## Lexique

Nom et utilité des pricipaux composants externes :

-   _Kunena_ : le système de forum
-   _CommunityBuilder_ : gestion des utilisateurs
-   _AcyMailing_ : gestion des lettres de diffusion
-   _!CAgenda_ : gestion du calendrier (programme)
-   _ChronoForms_ : pages personnalisées
-   _phpBB_ : ancien système de forum

