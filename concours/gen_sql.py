#!/usr/bin/env python3

import csv
import sys
import os.path
from datetime import date

FILENAME = 'notes.csv'
if __name__ == '__main__':
    if not os.path.isfile(FILENAME):
        print("Le fichier « {} » n'a pas été trouvé dans le répertoire courant.".format(FILENAME))
        sys.exit(1)

    with open(FILENAME, 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        pic_id = 0
        for row in csv_reader:
            voter_id = 1000000000
            for mark in row:
                print("INSERT INTO contest_marks VALUES('', '{:%Y-%m-%d}', '{}', '{}', '{}');".format(
                    date.today(), pic_id, voter_id, mark))
                voter_id += 1
            pic_id += 1
