#!/usr/bin/env python3

import glob
import sys
import os
from random import shuffle
import csv
import shutil
import base64
import unicodedata


SORTED_PATH = '_liste'
PREPARING_PATH = '_collage'
CSV_PATH = 'concours.csv'
LIST_PATH = 'list.txt'
FILE_TPL = '{0:03d}.jpg'
HEADERS = ['Rang', 'Id', 'Nom', 'Fichier', 'Total', 'Moyenne', 'Min', 'Max', 'Ecart type']
DELIM = ';'
BLANK_GIF = 'R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw=='


def usage():
    print('''
UTILISATION

    python3 concours.py REP


    REP est le chemin vers le répertoire contenu dans l'archive téléchargée sur
    le site.

      - Si REP contient déjà un fichier `concours.csv` et un répertoire `_liste`
    le fichier CSV est supposé contenir le classement, et les images sont
    renommées en conséquence.
      - Sinon le répertoire `_liste` est créé dans lequel sont placées les
    images numérotées aléatoirement et le fichier CSV est préparé pour la
    notation.

GÉNÉRATION DE LA LISTE DES IMAGES

    Lancer le script avec comme paramètre le le chemin vers le répertoire
    contenu dans l'archive téléchargée sur le site.

    Celui-ci va créer :
      - un répertoire `_liste` contenant toutes les images du concours et
      renommées selon le modèle : ID_AUTEUR_TITRE.jpg, l'ID étant le numéro
      attribué aléatoirement à l'image par le script ;
      - un fichier `concours.csv` prêt à recevoir les notes pour le
      dépouillement.

FICHIER CONCOURS.CSV

    Le fichier CSV généré comporte 9 colonnes :
      - Rang : le classement de la photo
      - Id : l'identifiant de la photo issu de la numérotation aléatoire lors
        de la première utilisation du script
      - Nom : l'auteur de la photo
      - Fichier : le nom du fichier original
      - Total : le nombre total de points recueillis par la photo
      - Moyenne : la note moyenne sur 20
      - Min : la note la plus basse
      - Max : la note la plus haute
      - Ecart type : l'écart type des notes

ÉTABLISSEMENT DU CLASSEMENT

    Une fois le fichier CSV généré et les notes recueillies l'importer dans
    un tableur avec comme encodage UTF-8 et `;` comme séparateur.

    1. Copier/coller les notes après la dernière colonne. Les formules dans les
    colonnes total, moyenn, min, max et écart type se calculent automatiquemnt.

    2. Trier le tableau selon la colonne `Total` en ordre décroissant.

    3. Entrer `1` dans la cellule A2.

    4. Ajouter un `=` devant la formule de la cellule A3 et recopier la
    formule vers le bas.

    5. Enregistrer le fichier CSV et lancer un nouvelle fois le script : les
    photos sont préfixées avec leur ordre d'arrivée au concours.

''')


def capitalize_names(name):
    """
    Takes a string as input, capitalizes the first letter of each word and puts
    the remainin to lowercase. Different from `capwords` in the sense that
    special proper noun characters are taken into account
    """
    capitalized_name = []
    for i in range(0, len(name)):
        if i == 0 or name[i-1] in " -'.":
            capitalized_name.append(name[i].upper())
        else:
            capitalized_name.append(name[i].lower())
    return ''.join(capitalized_name)


def has_consecutive_authors(names):
    for i in range(len(names)-1):
        if names[i].split(os.sep)[0] == names[i+1].split(os.sep)[0]:
            return True
    return False

def main():
    if len(sys.argv) == 1:
        usage()
        sys.exit(0)

    if len(sys.argv) != 2:
        print("Nombre de paramètres invalide", file=sys.stderr)
        usage()
        sys.exit(1)

    if not os.path.isdir(sys.argv[1]):
        print("Le chemin fourni n'est pas un répertoire existant", file=sys.stderr)
        sys.exit(1)

    os.chdir(sys.argv[1])

    # Entering this branch if the ranking has already been established
    if os.path.isdir(SORTED_PATH) and os.path.isfile(CSV_PATH):
        print("Liste des résultats trouvée.")
        print("Sauvegarde de la liste...")

        # Checking existence of backup directory
        backup_path = SORTED_PATH + '.bak'
        if os.path.isdir(backup_path):
            ans = input("Un répertoire de sauvegarde existe déjà, le supprimer et continuer ? [o/N] ")
            if ans.upper() != 'O':
                sys.exit(2)
            shutil.rmtree(backup_path)
        shutil.copytree(SORTED_PATH, backup_path)

        # Reading CSV and renaming
        print("Classement des fichiers...")
        with open(CSV_PATH, 'r') as csvfile:
            csvreader = csv.reader(csvfile, delimiter=DELIM)
            next(csvreader) # skipping

            os.chdir(SORTED_PATH)
            for row in csvreader:
                print('  #{:02d} [{:03d}] {} - {}'.format(int(row[0]), int(row[1]), *row[2:4]))
                filename = FILE_TPL.format(int(row[1]))
                os.rename(filename, '{0:03d}_{1}'.format(int(row[0]), filename))

        # Generating blank image for start screen (already in SORTED_PATH)
        with open('999_000_blank.gif', 'wb') as blank:
            blank.write(base64.b64decode(BLANK_GIF))
        print("Fait.")

    # Error branch if missing CSV or pictures directory
    elif os.path.isdir(SORTED_PATH) or os.path.isfile(CSV_PATH):
        print("Le répertoire n'est pas propre, veuillez le nettoyer et recommencer.", file=sys.stderr)
        sys.exit(1)

    # This branch generates the CSV file from the downloaded pictures
    else:
        pic_list = glob.glob('*/*.jpg')

        print('{0} photos trouvées...'.format(len(pic_list)))
        print('Mélange des photos...', end='')
        while has_consecutive_authors(pic_list):
            shuffle(pic_list)
            print('.', end='')
        print('.')

        print('Création de la liste...')
        pic_id = 0
        with open(CSV_PATH, 'w') as csvfile, open(LIST_PATH, 'w') as lstfile:
            csvwriter = csv.writer(csvfile, delimiter=DELIM, lineterminator='\n')
            csvwriter.writerow(HEADERS)
            os.mkdir(SORTED_PATH)
            os.mkdir(PREPARING_PATH)

            for pic in pic_list:
                pic = unicodedata.normalize('NFC', pic)
                lstfile.write(pic + '\n')
                pic_id += 1
                author, title = pic.split(os.sep)
                author = capitalize_names(author)
                marks_range = '($J{0}:$AZ{0})'.format(pic_id + 1)
                sum_formula = '=SOMME' + marks_range
                avg_formula = '=MOYENNE' + marks_range
                min_formula = '=MIN' + marks_range
                max_formula = '=MAX' + marks_range
                stddev_formula = '=ECARTYPE.STANDARD' + marks_range
                rank_formula = '$A2+SI($E3=$E2;0;NB.SI($E:$E;$E2))'
                csvwriter.writerow([rank_formula, pic_id, author, title, sum_formula, avg_formula, min_formula, max_formula, stddev_formula])

                print('  [{:03d}] {} - {}'.format(pic_id, author, title))

                shutil.copyfile(pic, os.path.join(SORTED_PATH, FILE_TPL.format(pic_id)))
                os.symlink(os.path.join('..', pic), os.path.join(PREPARING_PATH, f'{author} - {pic_id:03d}.jpg'))
        print('Fait.')


if __name__ == '__main__':
    main()
