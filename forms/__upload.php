<?

function get_err_str($code) {
     switch ($code) {
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            $message = "Le fichier est trop gros";
            break;
        case UPLOAD_ERR_PARTIAL:
            $message = "Le fichier n'a pas été téléchargé en entier";
            break;
        case UPLOAD_ERR_NO_FILE:
            $message = "Aucun fichier n'a été envoyé";
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
            $message = "Pas de dossier temporaire sur le serveur";
            break;
        case UPLOAD_ERR_CANT_WRITE:
            $message = "Échec d'enregistrement du fichier sur le serveur";
            break;
        case UPLOAD_ERR_EXTENSION:
            $message = "Une extension PHP à empêché l'envoi du ficher";
            break;
        default:
            $message = "Erreur lors de l'envoi (code $code)";
            break;
        }
    return $message;
}


$max_file_size_mb = 3;
$max_file_size = $max_file_size_mb * 1024 * 1024;
$error = '';


if (!empty($_POST['submit'])) {
    // the form has been submitted


    if ($_FILES['image']['error'] == 0) {
        $source = $_FILES['image']['tmp_name'];
        $destination = $_SESSION['fullpath'].'/'.str_replace(array(':','/'), ' ', pathinfo($_FILES['image']['name'])['filename'].'.jpg');

        if (move_uploaded_file($source, $destination)) {
            header('Location: index.php?option=com_chronoforms&chronoform=__thumbs');
        } else {
            $error = 'Erreur de traîtement du fichier';
        }
    } else {
        $error = get_err_str($_FILES['image']['error']);
    }
}
?>


<? if (!empty($error)): ?>
<p class="error"><?= $error ?></p>
<? endif; ?>

<h1>Téléchargement des images</h1>

<p class="important">IMPORTANT&nbsp;: à lire avant d'essayer de télécharger des images</p>
<p>Les images que vous pouvez télécharger doivent impérativement&nbsp;:</p>
<ul class="important">
    <li>être au format JPEG ;</li>
    <li>ne pas faire plus de <?= $max_file_size_mb ?>Mo (au besoin réduisez la taille ou augmentez la compression).</li>
</ul>


<form id="upload" action="?option=com_chronoforms&chronoform=__upload" enctype="multipart/form-data" method="post">
    <input type="hidden" name="MAX_FILE_SIZE" value="<?= $max_file_size ?>">

    <p><input type="file" name="image" /> <input type="submit" name="submit" value="Télécharger" class="btn btn-primary" /></p>
</form>
