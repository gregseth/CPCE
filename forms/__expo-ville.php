<?
$files = glob('cpce/expo-ville/*.[jJ][pP][gG]');

include('cpce/libcpce.php');
?>

<h1>La ville en citations</h1>

<ul id="user-thumbs">
<? foreach ($files as $img): ?>
    <? $name = format_name(str_replace('-', ' ', explode('---', basename($img))[0])); ?>
    <li class="image">
    <a href="<?= $img ?>" data-sub-html=".caption"><img src="cpce/resize.php?size=500&img=<?= urlencode(realpath($img)) ?>" />
    <div class="caption">
        <p class="title"><?= $name ?></p>
    </div>
    </a>
    <h4><?= $name ?></h4>
    </li>
<? endforeach; ?>
</ul>
<script type="text/javascript">
    lightGallery(document.getElementById('user-thumbs'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: '.image a',
        hideBarsDelay: 500,
        download: false
    });
</script>
