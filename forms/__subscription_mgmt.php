<?php

include('cpce/libcpce.php');

$db = &JFactory::getDBO();

$table = $db->getPrefix().'comprofiler';
$tjoomla = $db->getPrefix().'users';
$tablegp = $db->getPrefix().'user_usergroup_map';

$subscription_fee = 84;
?>


<h1>Gestion des inscriptions</h1>

<h2>Inscrits en attente de validation</h2>
<?
$qry = "SELECT user_id, lastname, firstname, city, registerDate FROM $table as p, $tjoomla as u "
    ." WHERE u.id=p.user_id AND approved<>1";
$db->setQuery($qry);
$result = $db->loadAssocList();
?>

<? if (!empty($result)): ?>
<form action="/gestion-inscriptions?event=do" method="POST">
<input type="hidden" name="action" value="activate">
<table class="list">
    <tr>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Ville</th>
        <th>Date d'inscription</th>
        <th></th>
    </tr>
    <? foreach ($result as $new_user): ?>
    <tr>
        <td><?= format_name($new_user['lastname']) ?></td>
        <td><?= format_name($new_user['firstname']) ?></td>
        <td><?= format_name($new_user['city']) ?></td>
        <td><?= date('Y-m-d', strtotime($new_user['registerDate'])) ?></td>
        <td><button type="submit" name="user" value="<?= $new_user['user_id'] ?>" class="btn btn-success">Activer le compte</button></td>
    </tr>
    <? endforeach; ?>
</table>
</form>
<? else: ?>
<p>Aucun.</p>
<? endif; ?>


<h2>Membres</h2>
<p><a href="/gestion-inscriptions?event=csv" class="btn btn-primary">Télécharger la liste</a></p>

<p class="important">Les boutons de la colonne « Actions » ne doivent être utilisés
que par le trésorier lorsqu'il a en sa possession le chèque correspondant.</p>

<table id="members" class="list">
    <tr>
        <th class="memberbox">📨</th>
        <th>Nom</th>
        <th>Prénom</th>
        <th>Ville</th>
        <th>Cotisation</th>
        <th>FPF</th>
        <th>Actions</th>
    </tr>

<?php
$total_paid = 0;
$total_fpf = 0;

$qry = "SELECT u.id, firstname, lastname, email, city, cb_lastcotiz, cb_adherentFPF "
      ."FROM `$tjoomla` AS u, `$table` AS p "
      ."WHERE u.id = p.user_id AND u.block = 0 "
      // group 18 is the "special" group for test members that should not appear here
        ."AND u.id NOT IN (SELECT user_id FROM $tablegp WHERE group_id = 18) "
      ."ORDER BY lastname ASC";
echo "<!-- $qry -->";

$db->setQuery($qry);
$results = $db->loadAssocList();
$total_subscribers = 0;

foreach ($results as $member):
    $member_id = $member['id'];
    $user_class = array('member');
    // 10 is the group id of the "bureau" group
    if (in_array(10, JFactory::getUser($member_id)->getAuthorisedGroups())) {
        $user_class[] = 'office';
    }

    $has_paid = $member['cb_lastcotiz'] == $season_start;
    $has_paid_fpf = $member['cb_adherentFPF'] == $season_start;
    $email_string = /*format_name($member['firstname'].' '.$member['lastname']).'<'.*/$member['email']/*.'>'*/;

    echo "<!-- $season_start : ".$member['cb_lastcotiz']." / ".$member['cb_adherentFPF']." -->";

    if ($has_paid) {
         $total_paid++;
         $user_class[] = 'paid';
    } else {
        $user_class[] = 'notpaid';
    }
    if ($has_paid_fpf) {
        $total_fpf++;
        $user_class[] = 'fpf';
    } else {
        $user_class[] = 'nofpf';
    }
    $total_subscribers++;
?>
    <tr>
        <td class="memberbox"><input type="checkbox" value="<?= $email_string ?>" class="<?= join(' ', $user_class) ?>"></td>
        <td>
            <? if ($has_paid): ?>
                <a href="https://photo-club-ermont.fr/index.php?option=com_chronoforms&tmpl=component&chronoform=__ballot&user_id=<?= $member_id ?>"><?= format_name($member['lastname']); ?></a>
            <? else: ?>
                <?= format_name($member['lastname']); ?>
            <? endif; ?>
        </td>
        <td><?= format_name($member['firstname']); ?> </td>
        <td><?= format_name($member['city']); ?> </td>
        <td class="color <?= $has_paid ? 'yes' : 'no' ?>"><?= $has_paid ? 'oui' : 'non' ?></td>
        <td class="<?= $has_paid_fpf ? 'yes' : 'no' ?>"><?= $has_paid_fpf ? 'oui' : 'non' ?></td>
        <td>
            <form action="/gestion-inscriptions?event=do" method="POST">
                <input type="hidden" name="user" value="<?= $member_id ?>">
                <button type="button" class="btn btn-primary" onclick="document.getElementById('payment-member<?= $member_id ?>').style.display='block'" <?= $has_paid ? 'disabled':'' ?>>Enregistrer paiement</button>
                <div id="payment-member<?= $member_id ?>" class="payment-details">
                    Montant&nbsp;:
                    <input type="radio" name="fee" id="sub_full<?= $member_id ?>" value="<?= $subscription_fee ?>" checked>
                    <label for="sub_full<?= $member_id ?>">Plein tarif (<?= $subscription_fee ?>€)</label>
                    <input type="radio" name="fee" id="sub_half<?= $member_id ?>" value="<?= $subscription_fee/2 ?>">
                    <label for="sub_half<?= $member_id ?>">Demi tarif (<?= $subscription_fee/2 ?>€)</label>
                    <input type="radio" name="fee" id="sub_free<?= $member_id ?>" value="0">
                    <label for="sub_free<?= $member_id ?>">Offert</label>
                    <div class="payment-validation">
                        <button type="submit" name="action" value="payment" class="btn btn-primary">Valider</button>
                        <button type="reset" class="btn btn-primary" onclick="document.getElementById('payment-member<?= $member_id ?>').style.display='none'">Annuler</button>
                    </div>
                </div>
                <button type="submit" name="action" value="fpf" class="btn btn-primary" <?= $has_paid_fpf ? 'disabled':'' ?>>Enregistrer paiement FPF</button>
                <button type="submit" name="action" value="disable" class="btn btn-danger" <?= $has_paid ? 'disabled':'' ?>>Désinscription</button>
            </form>
        </td>
        </td>
    </tr>
<? endforeach; ?>
</table>
<p id="select">Sélection :
    <button type="button" class="btn btn-primary" onclick="check_members('all')">Tous</button>
    <button type="button" class="btn btn-primary" onclick="check_members('')">Aucun</button>
    <button type="button" class="btn btn-primary" onclick="check_members('office')">Bureau</button>
    <button type="button" class="btn btn-primary" onclick="check_members('paid')">Cotisé</button>
    <button type="button" class="btn btn-primary" onclick="check_members('notpaid')">Non cotisé</button>
    <button type="button" class="btn btn-primary" onclick="check_members('fpf')">FPF</button>
    <button type="button" class="btn btn-warning" onclick="sendmail()">📨 Envoyer un mail aux membres sélectionnés</button>
</p>

<p id="summary">Nombre d'adhérents : <strong><?= $total_subscribers; ?></strong>
<br>Cotisations à jour : <strong><?= $total_paid; ?></strong>
<br>Adhérents FPF : <strong><?= $total_fpf; ?></strong></p>
