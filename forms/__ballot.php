<?php

include('cpce/libcpce.php');
// loading Community Builder API
include_once( JPATH_ADMINISTRATOR . '/components/com_comprofiler/plugin.foundation.php' );

$user_id = $_GET['user_id'];
$cbuser = CBuser::getUserDataInstance( $user_id );

?>
<html>
<head>
     <meta charset="utf-8">
<style>
@page {
    size: A4;
    margin: 2cm;
}
body {
    width: 17cm;
    margin: auto;
    padding: 2cm;
    font-family: 'Helvetica', 'Arial', sans-serif;
    font-size: 11pt;
}
h1 {
    text-align: center;
    font-size: 1.5em;
    font-weight: normal;
    font-variant: small-caps;
}
h1 img {
    width: 300px;
    height: auto;
    margin-right: 3em;
    margin-top: -7px;
    vertical-align: middle;
}
dl {
    margin-left: 3em;
}
dd {
    margin-top: -1.1em;
    margin-left: 11em;
}
dd:before {
    content: ': ';
}
.date {
    margin-left: 25em;
    margin-bottom: 7em;
}
.price {
    font-weight: bold;
}
.legal {
    text-align: center;
}
.public:after {
    content: '*';
}
footer {
    text-align: justify;
    font-size: .8em;
}
</style>
</head>
<body>
<h1><img src="https://photo-club-ermont.fr/images/logo/banner.png"  alt="Logo du CPCE">Bulletin d'adhésion</h1>

<p>Je soussigné</p>

<dl>
    <dt class="public">Nom</dt><dd><?= format_name($cbuser->lastname) ?></dd>
    <dt class="public">Prénom</dt><dd><?= format_name($cbuser->firstname) ?></dd>
    <dt>Adresse</dt><dd><?= strtolower($cbuser->cb_address) ?> <?= $cbuser->zipcode ?> <?= ucwords($cbuser->city) ?></dd>
    <dt>Téléphone</dt><dd><?= format_phone($cbuser->phone) ?></dd>
    <dt>Adresse électronique</dt><dd><?= strtolower($cbuser->email) ?></dd>
    <dt>Né(e) le</dt><dd><?= date('d/m/Y', strtotime($cbuser->cb_datedenaissance)) ?> à <?= ucwords($cbuser->cb_villedenaissance) ?></dd>
    <dt>Département (ou pays)</dt><dd><?= ucwords($cbuser->cb_codenaissance) ?></dd>
</dl>

<p>Adhérent n° <?= str_pad($user_id, 5, "0", STR_PAD_LEFT) ?></p>

<p>Après avoir pris connaissance des statuts et du règlement intérieur, demande
  mon adhésion au CINE PHOTO CLUB D'ERMONT, en qualité de «&nbsp;membre actif&nbsp; ».</p>

<p>Conformément à l'article 4 des statuts, alinéa a, je règle le montant de ma
  cotisation fixé à <span class="price"><?= $cbuser->cb_cotiz ?>&nbsp;€</span>.</p>

<p class="date">Fait à Ermont, le 01/10/<?= $cbuser->cb_lastcotiz ?><br>Signature :</p>

<p class="legal">Agréé par le Ministère de la Jeunesse et des Sports sous le n°2544.</p>

<footer>Les informations recueillies sont nécessaires pour votre adhésion.
  Elles font l'objet d'un traitement informatique et sont destinées au
  secrétariat de notre association. Elles peuvent donner lieu à l'exercice
  du droit d'accès et de rectification selon les dispositions de la loi du 6
  janvier 1978. Si vous souhaitez exercer ce droit et obtenir communication
  des informations vous concernant, veuillez vous adresser au président en
  exercice. Les champs suivis d’une * seront portés à la connaissance des
  membres de l’association sauf demande contraire de votre part.</footer>
</body>
</html>

<? exit(); ?>
