function setMark(id, value) {
    (function($) {
        $("#item_"+id)
            .removeClass('submit-fail')
            .removeClass('sumbit-ok')
            .removeClass('submit-missing');

        $.get('/', {
            'option': 'com_chronoforms',
            'chronoform': '__bestof',
            'event': 'submit',
            'id': id,
            'mark': value
        }).done(function(data) {
            // assertig error if any data printed
            if (data) alert(data);
            $("#item_"+id).addClass('submit-ok');
        }).fail(function() {
            alert('Erreur de communication avec le serveur.\nEssayez de recharger la page.');
            $("#item_"+id).addClass('submit-fail');
        });

    })(jQuery);
}

function saveMarks() {
    (function($) {
        missing_marks = [];
        $('[name^=pic_id_]').each(function (id) {
            if (this.value == 0) {
                missing_marks.push(id+1);
                $("#item_"+id).addClass('submit-missing');
            }
        });

        if (missing_marks.length == 0) {
            $.get('/', {
                'option': 'com_chronoforms',
                'chronoform': '__bestof',
                'event': 'submit',
                'save': 'true'
            }).done(function(data) {
                // assertig error if any data printed
                if (data) alert(data);
                $('#submit')
                    .removeClass('btn-danger')
                    .addClass('btn-success')
                    .text('✔︎ Vote enregistré');
            }).fail(function() {
                alert('Erreur de communication avec le serveur.\nRechargez la page et réessayez.');
                $('#submit')
                    .addClass('btn-danger')
                    .text('✘ Erreur');
            });
        } else {
            if (missing_marks.length == 1) {
                alert('La note de l\'image '+missing_marks+' est manquante');
            } else {
                alert('Les notes des images '+missing_marks.join(', ')+' sont manquantes');
            }
            $('html, body').animate({
                scrollTop: $("#item_"+missing_marks[0]).offset().top - 600
            }, 500);
        }
    })(jQuery);
}