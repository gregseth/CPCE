function validateMarks() {
    one = document.getElementById('one').value;
    two = document.getElementById('two').value;
    three = document.getElementById('three').value;
    if (one == -1 || two == -1 || three == -1) {
        alert('Vous devez choisir vos trois photos préférées.');
        return false;
    }
    if  (one == two || two == three ||  three == one) {
        alert('Vous ne pouvez pas donner deux fois la même note à une photo.');
        return false;
    }
    return true;
}