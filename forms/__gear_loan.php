<?php
include('cpce/libcpce.php');

const OFFICE_GROUP_ID = 10;
const MEMBERS_GROUP_ID = 9;

$db = &JFactory::getDBO();
$prefix = $db->getPrefix();
$current_user = JFactory::getUser()->id;

//print_r($_POST);
//echo "<pre> User ID".JFactory::getUser()->id."</pre>";

if (!empty($_POST['action']))
switch ($_POST['action']) {
    case 'add':
        $db->setQuery("INSERT INTO gear (name, serial, deposit) "
            ."VALUES ('{$_POST['name']}', '{$_POST['serial']}', '{$_POST['deposit']}')");
        $db->execute();
        break;
    case 'borrow':
        $db->setQuery("INSERT INTO gear_loan (id_item, id_member, loan_date, id_office_loan) "
            ."VALUES ('{$_POST['gear_id']}', '{$_POST['borrower']}', NOW(), '{$current_user}')");
        $db->execute();
        $db->setQuery("UPDATE gear "
            ."SET borrowed = 1 "
            ."WHERE id = '{$_POST['gear_id']}'");
        $db->execute();
        break;
    case 'return':
        $db->setQuery("UPDATE gear_loan "
            ."SET return_date = NOW(), id_office_return = '{$current_user}' "
            ."WHERE id_item = '{$_POST['gear_id']}' AND return_date IS NULL");
        $db->execute();
        $db->setQuery("UPDATE gear "
            ."SET borrowed = 0 "
            ."WHERE id = '{$_POST['gear_id']}'");
        $db->execute();
        break;
    case 'delete':
        $db->setQuery("DELETE FROM gear "
            ."WHERE id = '{$_POST['gear_id']}'");
        $db->execute();
        $db->setQuery("DELETE FROM gear_loan "
            ."WHERE id_item = '{$_POST['gear_id']}'");
        $db->execute();
        break;
}

$qry = "SELECT u.id, name "
    ."FROM `${prefix}users` AS u "
    ."WHERE u.block = 0 "
        ."AND u.id NOT IN (SELECT user_id FROM `${prefix}user_usergroup_map` WHERE group_id IN (18)) "
    ."ORDER BY name ASC";
echo "<!-- $qry -->";
$db->setQuery($qry);
$members = $db->loadAssocList();

$qry = "SELECT id, name, serial, deposit "
   ."FROM `gear`"
   ."WHERE borrowed = 0";
echo "<!-- $qry -->";
$db->setQuery($qry);
$available = $db->loadAssocList();

$qry = "SELECT gear.id, gear.name, gear.serial, gear_loan.loan_date AS date, u.name AS borrower, o.name AS loaner "
    ."FROM gear_loan "
        ."LEFT JOIN gear ON gear_loan.id_item = gear.id "
        ."LEFT JOIN ${prefix}users u on gear_loan.id_member = u.id "
        ."LEFT JOIN ${prefix}users o on gear_loan.id_office_loan = o.id "
    ."WHERE return_date IS NULL";
echo "<!-- $qry -->";
$db->setQuery($qry);
$borrowed = $db->loadAssocList();

$qry = "SELECT g.name AS name, serial, loan_date, return_date, u.name AS borrower, o.name AS loaner, r.name AS returner "
    ."FROM `gear` AS g, `gear_loan` AS l, `${prefix}users` AS u, `${prefix}users` AS o, `${prefix}users` AS r "
    ."WHERE g.id = l.id_item AND l.id_member = u.id AND l.id_office_loan = o.id AND l.id_office_return = r.id "
    ."ORDER BY loan_date DESC";
echo "<!-- $qry -->";
$db->setQuery($qry);
$history = $db->loadAssocList();
?>

<h2>Prêt de matériel</h2>
<h3>Ajout de nouveau matériel</h3>
<form action="/gestion-materiel" method="post">
    <label for="name">Nom</label>
    <input type="text" name="name" id="name" required><br>
    <label for="serial">Numéro de série</label>
    <input type="text" name="serial" id="serial" required><br>
    <label for="deposit">Montant de la caution</label>
    <input type="number" name="deposit" id="deposit" required><br>
    <button type="submit" name="action" value="add" class="btn btn-success">Ajouter</button>
</form>

<h3>Matériel disponible</h3>
<table>
    <tr>
        <th>Nom</th>
        <th>Numéro de série</th>
        <th>Emprunteur</th>
        <th>Caution</th>
        <th>Action</th>
    </tr>
    <? foreach ($available as $gear): ?>
    <tr>
    <form action="/gestion-materiel" method="post">
    <input type="hidden" name="gear_id" value="<?= $gear['id'] ?>">
        <td><?= $gear['name'] ?></td>
        <td><?= $gear['serial'] ?></td>
        <td>
            <select name="borrower" id="borrower_<?= $gear['id'] ?>" onchange="toggle_borrow(<?= $gear['id'] ?>, this.value)">
                <option value="0">-- Choisir un membre --</option>
                <? foreach ($members as $m): ?>
                <option value="<?= $m['id'] ?>"><?= format_name($m['name']) ?></option>
                <? endforeach; ?>
            </select>
        </td>
        <td><?= $gear['deposit'] ?> €</td>
        <td>
            <button type="submit" id="borrow_<?= $gear['id'] ?>" name="action" value="borrow" class="btn btn-primary" disabled>Emprunter</button>
            <button type="submit" id="delete_<?= $gear['id'] ?>" name="action" value="delete" class="btn btn-danger"  onclick="return confirm_delete('<?= $gear['name'] ?> - <?= $gear['serial'] ?>', this)">Supprimer</button>
        </td>
    </form>
    </tr>
    <? endforeach; ?>
</table>

<h3>Matériel emprunté</h3>
<table>
    <tr>
        <th>Nom</th>
        <th>Numéro de série</th>
        <th>Emprunté par</th>
        <th>Date de prêt</th>
        <th>Prêté par</th>
        <th>Action</th>
    </tr>
    <? foreach ($borrowed as $gear): ?>
    <form action="/gestion-materiel" method="post">
    <input type="hidden" name="gear_id" value="<?= $gear['id'] ?>">
    <tr>
        <td><?= $gear['name'] ?></td>
        <td><?= $gear['serial'] ?></td>
        <td><?= format_name($gear['borrower']) ?></td>
        <td><?= date('d-m-Y', strtotime($gear['date'])) ?></td>
        <td><?= format_name($gear['loaner']) ?></td>
        <td><button type="submit" name="action" value="return" class="btn btn-primary">Retourner</button></td>
    </tr>
    </form>
    <?php endforeach; ?>
</table>

<h3>Historique des prêts</h3>
<table>
    <tr>
        <th>Nom</th>
        <th>Numéro de série</th>
        <th>Emprunté par</th>
        <th>Date de prêt</th>
        <th>Prêté par</th>
        <th>Date de retour</th>
        <th>Retour validé par</th>
    </tr>
    <? foreach ($history as $hist): ?>
    <tr>
        <td><?= $hist['name'] ?></td>
        <td><?= $hist['serial'] ?></td>
        <td><?= format_name($hist['borrower']) ?></td>
        <td><?= date('d-m-Y', strtotime($hist['loan_date'])) ?></td>
        <td><?= format_name($hist['loaner']) ?></td>
        <td><?= date('d-m-Y', strtotime($hist['return_date'])) ?></td>
        <td><?= format_name($hist['returner']) ?></td>
    </tr>
    <? endforeach; ?>
</table>
