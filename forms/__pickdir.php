<?php

$form_action = '__thumbs';
$text_action = 'télécharger';
$text_title = 'Téléchargement';
if (!empty($_GET['action']) && $_GET['action']=='download') {
    $form_action = '__download';
    $text_action = 'récupérer';
    $text_title = 'Récupération';
}

$include = function($needle) use($form_action) {
    // excluded patterns:
    //   - anythiing starting with a .
    //   - the « default » folder
    //   - for the upload page:
    //     - any folder dated after the current date
    //     - any folder dated at the current date past 20h
    return substr($needle, 0, 1) != '.'
        &&  $needle != 'default'
        &&  (   $form_action == '__download'
            ||  date('Y.m.d') < substr($needle, 0, 10)
            ||  (date('Y.m.d') == substr($needle, 0, 10) && date('H') < 20)
            );
};
$list = array_filter(scandir('cpce/upload'), $include);

// DEBUG:
// print_r($list);

?>

<h1><?= $text_title ?> d'images</h1>

<form action="?" method="get">
<input type="hidden" name="option" id="option" value="com_chronoforms" />
<input type="hidden" name="chronoform" id="chronoform" value="<?= $form_action ?>" />
<p>Choisissez l'événement pour lequel <?= $text_action ?> les images :</p>
<p>
    <select name="reason" id="reason">
    <? if ($form_action == '__thumbs'): ?>
        <option value="default" selected>Galerie personnelle</option>
    <? endif; ?>
    <? foreach ($list as $dir): ?>
        <option value="<?= $dir ?>"><?= $dir ?></option>
    <? endforeach; ?>
    </select>
    <button class="button cbe-button" type="submit"> OK </button>
</p>
</form>
