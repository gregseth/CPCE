<?
$db = JFactory::getDBO();
$sql_categories = "SELECT theme FROM bestof_marks WHERE year=2018 GROUP BY theme ORDER BY theme ASC";

$db->setQuery($sql_categories);
$categories = $db->loadColumn();

$cat_id = 0;
if (!empty($_GET['cat'])) {
    $cat_id = $_GET['cat'];
}
$cat = $categories[$cat_id];
$sql_cat = $db->quote($cat);

$path = 'cpce/upload_archives/'.$cat;
$picture_list = glob($path . '/*/*.[jJ][pP][gG]');


$sql = "SELECT picture_id, SUM(mark) AS total, AVG(mark) AS average, STD(mark) AS stddev, MIN(mark) AS min, MAX(mark) AS max FROM bestof_marks WHERE year=2018 AND theme=$sql_cat GROUP BY picture_id ORDER BY average DESC";
$db->setQuery($sql);
$results = $db->loadAssocList();
$rank = 1;
$previous_total = 0;
$exaequo_count = 0;
/*dbg:
echo '<pre>';
print_r($results);
echo '</pre>';
*/
?>

<h1>Notation florilège&nbsp;: résultats</h1>

<form action="vote-bestof-resultats" method="GET">
<select name="cat" onchange="this.form.submit()">
<? for ($i=0; $i<count($categories); $i++): ?>
    <option value="<?= $i ?>" <?= ($i == $cat_id) ? 'selected' : '' ?>><?= $categories[$i] ?></option>
<? endfor; ?>
</select>
</form>

<p class="commands">
    <button class="btn btn-primary" onclick="toggle('mark')">Notes</button>
    <button class="btn btn-primary" onclick="toggle('stats')">Statistiques</button>
</p>

<div id="container">
<table>
    <tr>
        <th></th>
        <th colspan="2">Image</th>
        <th>Auteur</th>
        <th>Total</th>
        <th>Moyenne</th>
        <th class="stats">Min</th>
        <th class="stats">Max</th>
        <th class="stats">Écart type</th>
        <th class="mark">Notes</th>
    </tr>
<? foreach ($results as $pic): ?>
<?
if ($pic['total'] < $previous_total) {
    $rank += $exaequo_count + 1;
    $exaequo_count = 0;
} elseif ($rank > 1) {
    $exaequo_count++;
}
$previous_total = $pic['total'];
$id = $pic['picture_id'];

$sql_marks = "SELECT mark, voter_id FROM bestof_marks WHERE year=2018 AND theme=$sql_cat AND picture_id=$id ORDER BY voter_id";
$db->setQuery($sql_marks);
$results_marks = $db->loadAssocList();
/*dbg:
echo '<pre>';
print_r($results_marks);
echo '</pre>';
*/
$split_path = explode('/', $picture_list[$pic['picture_id']]);

$author = $split_path[3];
$auth_std = strtolower(str_replace(' ', '-', $author));
//dbg: echo $author;
?>
    <tr class="table-result-row <?= $auth_std ?>">
        <td><?= $rank ?></td>
        <td>n°<?= $pic['picture_id']+1 ?></td>
        <td class="image"><a href="<?= $picture_list[$pic['picture_id']] ?>"><img src="cpce/resize.php?size=120&img=<?= urlencode(realpath($picture_list[$pic['picture_id']])) ?>" /></a></td>
        <td><a href="javascript:filterAuthor('<?= $auth_std ?>')"><?= $author ?></td>
        <td><?= $pic['total'] ?></td>
        <td><?= round($pic['average'], 2) ?></td>
        <td class="stats"><?= $pic['min'] ?></td>
        <td class="stats"><?= $pic['max'] ?></td>
        <td class="stats"><?= round($pic['stddev'], 1) ?></td>
    <? foreach ($results_marks as $marks): ?>
        <td class="mark">
            <!-- <?= JFactory::getUser($marks['voter_id'])->name ?> -->
            <?= $marks['mark'] ?>
        </td>
    <? endforeach; ?>
    </tr>
<? endforeach; ?>
</table>
</div>
