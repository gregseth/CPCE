function toggle(className) {
    (function($) {
        $('.'+className).toggle();
    })(jQuery);
}
function filterAuthor(author) {
    (function($) {
        $('.table-result-row').not('.'+author).toggle();
    })(jQuery);
}