<?php
setlocale(LC_ALL, "fr_FR");
date_default_timezone_set('Europe/Paris');

$db = & JFactory::getDBO();
$tb = 'icagenda_events';
$table = $db->getPrefix().$tb;
$is_logged_in = JFactory::getUser()->id > 0;

// if we're after september 1st, $year is current year, last year instead
$year = intval(date('Y')) - ((time() > strtotime(date('Y').'-09-01')) ? 0 : 1);

$season_start = $year.'-09-01';
$season_end = ($year+1).'-07-01';

$qry = "SELECT title, startdate, enddate, dates, displaytime, shortdesc, `desc` "
      ."FROM $table "
      ."WHERE startdate > '$season_start' AND startdate < '$season_end' AND state = 1 "
      ."ORDER BY startdate ASC";

$db->setQuery($qry);
$results = $db->loadAssocList();

$event_list = array();
foreach ($results as $event) {
  array_push($event_list, $event);
  if ($event['dates'] != 'a:1:{i:0;s:16:"0000-00-00 00:00";}') {
    preg_match_all('/"(\d\d\d\d-\d\d-\d\d) \d\d:\d\d"/', $event['dates'], $dates);
    foreach ($dates[1] as $date) {
      array_push($event_list, array(
        'title' => $event['title'],
        'startdate' => $date.' '.explode(' ', $event['startdate'])[1],
        'enddate' => $date.' '.explode(' ', $event['enddate'])[1],
        'displaytime' => $event['displaytime'],
        'shortdesc' => $event['shortdesc'],
        'desc' => $event['desc'],
      ));
    }
  }
}

$events_date = array_column($event_list, 'startdate');
array_multisort($events_date, SORT_ASC, $event_list);


$id = 0;
$double_event = 0;
?>

<h2>Programme <?= $year.'/'.($year+1) ?></h2>
<? if ($is_logged_in): ?>
<p style="text-align: right">
  <button id="toggle-past-events" class="btn btn-primary">Afficher les événements passés</button>
  <button id="toggle-ics-link" class="btn btn-primary">S'abonner (.ics)</button><input type="text" id="ics-link" style="display:none" value="https://photo-club-ermont.fr/programme-cpce" />
</p>
<? endif; ?>
<table id="program">
  <tr>
    <th class="date">Date</th>
    <th class="title">Sujet</th>
    <th class="desc">Description</th>
  </tr>

<?php
foreach ($event_list as $event):
  $row = 0;
  $title = $event['title'];
  $timestamp = strtotime($event['startdate']);

  $date_start = date("d/m/Y", $timestamp);
  $date_end = date("d/m/Y", strtotime($event['enddate']));
  $past_event = date("Ymd") > date("Ymd", $timestamp);
  $is_today = date("d/m/Y") == $date_start;
  $time_start = date("H:i", $timestamp);
  $time_end  = date("H:i", strtotime($event['enddate']));

  $display_date = ($date_start == $date_end) ? $date_start : $date_start.'<br>'.$date_end;

  $short_desc  = $event['shortdesc'];
  $desc = $event['desc'];

  do
  {
    // counting events happening at the same date
    $row++;
    $next_event = $event_list[$id + $row];
    $next_date = date("d/m/Y", strtotime($next_event['startdate']));
  } while($next_date == $date_start);

?>
  <tr <?= $past_event ? 'class="past"' : '' ?><?= $is_today ? 'class="today"' : '' ?> >
<? if ($double_event == 0): ?>
    <td class="date" rowspan="<?php echo $row; ?>"><?= $display_date ?></td>
<? endif; ?>
    <td class="title"><b><?= $title ?></b><br /><?= $event['displaytime'] ? $time_start." ".$time_end : ''; ?></td>
    <td class="desc">
      <?= !empty($short_desc) ? '<p class="summary">'.$short_desc.'</p>':''; ?>
      <?= ($is_logged_in && !empty($desc)) ? '<p>'.$desc.'</p>':''; ?>
    </td>
  </tr>
<?php
  // next event
  $id ++;
  // updating flag to show or not the date on the next line
  $double_event = ($row >= 2) ? 1 : 0;
endforeach;
?>
</table>
