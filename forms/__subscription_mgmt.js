$ = jQuery;
function check_members(selector) {
    if (selector == 'all') {
        $('.member').prop('checked', true);
    } else {
        $('.member').prop('checked', false);
        if (selector != '') {
            $('.member.'+selector).prop('checked', true);
        }
    }
}

function sendmail() {
    if ($('.member:checked').length > 0) {
        location.href = 'mailto:' + $('.member:checked').map(function() {
            return $(this).val();
        }).get().join(',');
    }
}
