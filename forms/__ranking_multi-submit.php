<?

$lockfile = 'cpce/vote_is_open_contest';
$is_vote_open = file_exists($lockfile);

function get_picture_id($mark_id) {
    return floor($mark_id / 3.);
}

//header('Content-Type: text/plain');
if ($is_vote_open) {
    $date = $_GET['date'];
    $voter = JFactory::getUser()->id;
    if (empty($voter) || $voter == 0) {
        exit("Vous devez être connecté pour voter.");
    }
//dbg: $date = '2019-05-31';
    if (!empty($_GET['save'])) {
        $null_marks = array();
        foreach ($_SESSION['marks_multi'] as $id => $marks) {
            foreach ($marks as $cat => $mark) {
                if ($mark == -1) {
                    $null_marks[] = $id+1;
                }
            }
        }
        if (!empty($null_marks)) {
            $missing = array_unique(array_map('get_picture_id', $null_marks));
            exit("Les notes des images ".join(", ", $missing)." ne peuvent être vides.\nMerci de les modifier.");
        }
        foreach ($_SESSION['marks_multi'] as $id => $marks) {
            foreach ($marks as $cat => $mark) {
                $sql = "REPLACE INTO contest_marks_multi VALUES('', '$date', '$id', '$voter', '$cat', '$mark');\n";
                $db = JFactory::getDBO();
                $db->setQuery($sql);
                $db->execute();
            }
        }
    } else {
        $id = (!empty($_GET['id'])) ? $_GET['id'] : 0;
        $mark = (!empty($_GET['mark'])) ? $_GET['mark'] : 0;
        $cat = (!empty($_GET['cat'])) ? $_GET['cat'] : 'tech';

        $_SESSION['marks_multi'][$id][$cat] = $mark;
    }

    exit();
} else {
    exit("Le vote est fermé !\nLa note ne sera pas prise en compte.");
}

?>