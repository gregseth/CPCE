<?
require_once('cpce/read_exif.php');
require_once('cpce/libcpce.php');

$lockfile = 'cpce/vote_is_open';
$is_vote_open = file_exists($lockfile);
$lockresults = 'cpce/hold_results';
$hold_results = file_exists($lockresults);
$is_admin = JFactory::getUser()->id == 173;

$sort = "average DESC, picture_id ASC";
$sort_link = array(
    'id' => '?sort=id',
    'avg' => '?sort=avg',
    'std' => '?sort=std',
    'min' => '?sort=min',
    'max' => '?sort=max'
);
$sort_class = array(
    'id' => '',
    'avg' => 'sorted',
    'std' => '',
    'min' => '',
    'max' => ''
);
$custom_sort = "";
$colspan_offset = 1;
if (!empty($_GET['sort'])) {
    switch ($_GET['sort']) {
        case 'id':      $sort = "picture_id ASC";                   break;
        case 'avg':     $sort = "average DESC, picture_id ASC";     break;
        case 'std':     $sort = "stddev DESC";                      break;
        case 'min':     $sort = "min ASC";                          break;
        case 'max':     $sort = "max DESC";                         break;
    }
    if ($_GET['sort'] != 'avg') {
        $custom_sort = "custom-sort";
        $sort_class['avg'] = '';
        $colspan_offset = 0;
    }
    $sort_link[$_GET['sort']] = '';
    $sort_class[$_GET['sort']] = 'sorted';
}

function get_author($path) {
    $split_path = explode('/', $path);
    return $split_path[array_key_last($split_path)-1];
}

$date_index = ((!$is_vote_open && !$hold_results) || $is_admin) ? 0 : 1;
$show_voter_name = $is_admin;

$db = JFactory::getDBO();
$sql_dates = "SELECT DATE_FORMAT(date, '%Y.%m.%d') FROM contest_marks GROUP BY date ORDER BY date DESC";

$db->setQuery($sql_dates);
$date_list = array_slice($db->loadColumn(), $date_index);

$date_dot = $date_list[0];
if (!empty($_GET['date'])) {
    $date_dot = $_GET['date'];
}
$display_path = "";
$select_data = array();
foreach ($date_list as $date) {
    $path = glob('cpce/upload/'.$date.'*')[0];
    if (empty($path)) {
        $path = glob('cpce/upload_archives/'.$date.'*')[0];
        if (empty($path)) {
            $path = glob('cpce/upload/.'.$date.'*')[0];
        }
    } else {
        if ($skip_first_in_upload && !$first_in_upload_skipped) {
            $first_in_upload_skipped = true;
            continue;
        }
    }

    $select_data[$date] = array_pop(explode('/', $path));

    if ($date == $date_dot) {
        $is_contest = (strpos(strtolower($path), '- concours -') === false) ? false : true;
        if ($is_contest) {
            $picture_list = array();
            foreach (array_filter(file($path.'/list.txt', FILE_IGNORE_NEW_LINES)) as $item) {
                $picture_list[] = $path.'/'.$item;
            }
        } else {
            $picture_list = glob($path . '/*/*.[jJ][pP][gG]');
        }
        $display_path = $path;
    }
}

$sql = "SELECT picture_id, SUM(mark) AS total, AVG(mark) AS average, STD(mark) AS stddev, MIN(mark) AS min, MAX(mark) AS max FROM contest_marks "
    ."WHERE date='$date_dot' "
    ."GROUP BY picture_id "
    ."ORDER BY $sort";
$db->setQuery($sql);
$results = $db->loadAssocList();
$rank = 1;

$nb_pic = count($results);
$author_list = array();
foreach($picture_list as $pic) {
    $author_list[] = explode('/', $pic)[3];
}
$author_list = array_unique($author_list);
$nb_authors = count($author_list);
$db->setQuery("SELECT DISTINCT voter_id FROM contest_marks WHERE date='$date_dot' ORDER BY voter_id");
$voters = $db->loadColumn();
$nb_voters = count($voters);

$previous_total = 0;
$exaequo_count = 0;

$summary = array(
    'picture_id' => array(),
    'rank' => array(),
    'voter_id' => array(),
    'marks' => array()
);
/*dbg:
echo '<pre>';
print_r($results);
echo '</pre>';
*/
?>

<form action="vote-resultats" method="GET">
<select name="date" onchange="this.form.submit()">
<? foreach ($select_data as $date => $title): ?>
    <option value="<?= $date ?>" <?= ($date == $date_dot) ? 'selected' : '' ?>><?= $title ?></option>
<? endforeach; ?>
</select>
</form>

<dl>
    <dt>Nombre de photos :</dt>
    <dd><?= $nb_pic ?></dd>
    <dt>Nombre de participants :</dt>
    <dd><?= $nb_authors ?></dd>
    <dt>Nombre de votants :</dt>
    <dd><?= $nb_voters ?></dd>
</dl>

<? if (!$is_contest): ?>
<div id="imglist">
<h2>Les trois favorites</h2>
<ul>
<? 
for ($i=0; $i<3; $i++): 
    $path = $picture_list[$results[$i]['picture_id']];
?>
    <li class="image favitem">
        <a href="<?= $picture_list[$results[$i]['picture_id']] ?>" data-sub-html=".caption">
            <img src="cpce/resize.php?size=400&img=<?= urlencode(realpath($path)) ?>" />
            <div class="caption">
                <p class="title"><?= get_author($path)." — ".get_title($path) ?></p>
                <p class="exif"><?= exif(realpath($path)) ?></p>
            </div>
        </a>
    </li>
<? endfor; ?> 
</ul>
<h2>Toutes les images</h2>
<ul>
<? foreach ($picture_list as $pic): ?>
    <li class="image listitem">
        <a href="<?= $pic ?>" data-sub-html=".caption">
            <img src="cpce/resize.php?size=120&img=<?= urlencode(realpath($pic)) ?>" />
            <div class="caption">
                <p class="title"><?= get_author($pic)." — ".get_title($pic) ?></p>
                <p class="exif"><?= exif(realpath($pic)) ?></p>
            </div>
        </a>
    </li>
<? endforeach; ?>
</ul>
</div>
<script type="text/javascript">
    lightGallery(document.getElementById('imglist'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: '.image a',
        hideBarsDelay: 1000,
        download: false
    });
</script>
<? endif; ?>

<? if ($is_contest or $is_admin): ?>
<p class="commands">
    <button class="btn btn-primary" onclick="toggle('mark')">Notes</button>
    <button class="btn btn-primary" onclick="toggle('stats')">Statistiques</button>
</p>

<div id="container">
<table>
    <tr>
        <th class="<?= $custom_sort ?>"></th>
        <th colspan="2" class="<?= $sort_class['id'] ?>"><a href="resultat-vote<?= $sort_link['id'] ?>">Image</a></th>
        <th>Auteur</th>
        <th>Total</th>
        <th class="<?= $sort_class['avg'] ?>"><a href="resultat-vote<?= $sort_link['avg'] ?>">Moyenne</a></th>
        <th class="stats <?= $sort_class['min'] ?>"><a href="resultat-vote<?= $sort_link['min'] ?>">Min</a></th>
        <th class="stats <?= $sort_class['max'] ?>"><a href="resultat-vote<?= $sort_link['max'] ?>">Max</a></th>
        <th class="stats <?= $sort_class['std'] ?>"><a href="resultat-vote<?= $sort_link['std'] ?>">Écart type</a></th>
        <th class="mark" colspan="<?= count($voters) ?>">Notes</th>
    </tr>
<? if ($show_voter_name): ?>
    <tr>
        <td colspan="<?= 5+$colspan_offset ?>"></td>
        <td colspan="3" class="stats"></td>
    <? foreach ($voters as $vid): ?>
        <? $summary['voter_id'][] = $vid; ?>
        <td class="mark voter<?=$vid?>" onclick="highlight(<?=$vid?>)"><?= ($vid < 1000000000) ? format_name(JFactory::getUser($vid)->name) : $vid ?></td>
    <? endforeach; ?>
    </tr>
<? endif; ?>
<? foreach ($results as $pic): ?>
<?
if ($pic['average'] < $previous_total) {
    $rank += $exaequo_count + 1;
    $exaequo_count = 0;
} elseif ($previous_total > 0) {
    $exaequo_count++;
}
$previous_total = $pic['average'];
$id = $pic['picture_id'];
$num = $id+1;
$summary['picture_id'][] = $num;
$summary['rank'][] = $rank;

$sql_marks = "SELECT mark, voter_id FROM contest_marks WHERE date='$date_dot' AND picture_id=$id ORDER BY voter_id";
$db->setQuery($sql_marks);
$results_marks = $db->loadAssocList();
/*dbg:
echo '<pre>';
print_r($results_marks);
echo '</pre>';
*/
$split_path = explode('/', $picture_list[$id]);

$author = $split_path[3];
$auth_std = strtolower(str_replace(' ', '-', $author));
//dbg: echo $author;
?>
    <tr class="table-result-row <?= $auth_std ?>">
        <td class="<?= $custom_sort ?>"><?= $rank ?></td>
        <td>n°<?= $num ?></td>
        <td class="image">
            <a href="<?= $picture_list[$id] ?>" data-sub-html=".caption">
                <img src="cpce/resize.php?size=120&img=<?= urlencode(realpath($picture_list[$id])) ?>" />
                <div class="caption">
                    <p class="title"><?= "#$rank (n°$num) — $author — ".get_title($picture_list[$id]) ?></p>
                    <p class="exif"><?= exif(realpath($picture_list[$id])) ?></p>
                </div>
            </a>
        </td>
        <td><a href="javascript:filterAuthor('<?= $auth_std ?>')"><?= format_name($author) ?></td>
        <td><?= $pic['total'] ?></td>
        <td><?= round($pic['average'], 2) ?></td>
        <td class="stats"><?= $pic['min'] ?></td>
        <td class="stats"><?= $pic['max'] ?></td>
        <td class="stats"><?= round($pic['stddev'], 1) ?></td>
    <? 
    $marks_for_pic = array();
    foreach ($results_marks as $marks):
        $voter_name = '';
        $vid = $marks['voter_id'];
        if ($show_voter_name && $vid < 1000000000) {  // filtering out fake users of manual notation
            $voter_name = format_name(JFactory::getUser($vid)->name);
        }
        $marks_for_pic[] = $marks['mark'];
    ?>
        <td class="mark voter<?=$vid?>">
            <span title="<?= $voter_name ?>"><?= $marks['mark'] ?></span>
        </td>
    <? 
    endforeach;
    $summary['marks'][] = $marks_for_pic;
    ?>
    </tr>
<? endforeach; ?>
    <tr id="last" style="display:none">
        <td colspan="10" class="image">
            <a href="images/black.png"><img src="images/black.png" /></a>
        </td>
    </tr>
</table>
</div>

<!-- <pre><? // print_r(json_encode($summary, JSON_NUMERIC_CHECK)); ?></pre> -->
<!--
<?php
/*
echo "summary = [\n";
for ($i=0; $i<count($summary['marks']); $i++) {
    for ($j=0; $j<count($summary['marks'][$i]); $j++) {
        echo "{ p: ".$summary['picture_id'][$i].", r: ".$summary['rank'][$i].", v: ".$summary['voter_id'][$j].", m: ".$summary['marks'][$i][$j]."},\n";
    }
}
echo "];\n";
*/
?>
-->
<? 
$stats_path = $display_path.'/stats_'.count($summary['voter_id']).'.png';
if (!file_exists($stats_path)) {
    # deleting all previous files, if any
    foreach(glob($display_path.'/stats*.png') as $f) {
        unlink($f);
    }
    $ch = curl_init('https://photo.gregseth.net/cpce/plot');
    $payload = json_encode($summary, JSON_NUMERIC_CHECK);
    $out = fopen($stats_path, 'wb');

    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FILE, $out); 

    curl_exec($ch);
    curl_close($ch);
    fclose($out);
}
?>
<div class="stats">
    <img src="<?= $stats_path ?>" />
</div>
<script type="text/javascript">
<? if (!empty($_GET['sort'])): ?>
    toggle('mark');
    toggle('stats');
<? endif; ?>
    lightGallery(document.getElementById('container'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: '.image a',
        hideBarsDelay: 1000,
        download: false
    });
</script>
<? endif; ?>

