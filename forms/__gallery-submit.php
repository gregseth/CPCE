<?
$user = $_GET['user'];

$db = &JFactory::getDBO();
$cb_users_table = $db->getPrefix().'comprofiler';
$sql = "SELECT website FROM $cb_users_table WHERE concat(firstname, ' ', lastname) = '$user';";

$db->setQuery($sql);
$res = $db->loadRow();
$website = '';
if (count($res)) {
    $website = $res[0];
}

$files = glob('cpce/upload/default/'.$user.'/*.[jJ][pP][gG]');

include('cpce/libcpce.php');
require_once('cpce/read_exif.php');
?>


<? if (!empty($user)): ?>
<h1><?= format_name($user) ?></h1>

<? if (!empty($website)): ?>
     <h4>Site&nbsp;: <a href="<?= (substr($website, 0, 4) == 'http' ? '' : 'http://'). $website ?>"><?= $website ?></a></h4>
<? endif; ?>


<ul id="user-thumbs">
<? foreach ($files as $img): ?>
    <li>
    <a href="<?= $img ?>" data-sub-html=".caption"><img src="cpce/resize.php?size=500&img=<?= urlencode(realpath($img)) ?>" />
    <div class="caption">
        <p class="title"><?= get_title($img) ?></p>
        <p class="exif"><?= exif(realpath($img)) ?></p>
    </div>
    </a>
    </li>
<? endforeach; ?>
</ul>
<script type="text/javascript">
    lightGallery(document.getElementById('user-thumbs'), {
        subHtmlSelectorRelative: true,
        selector: 'li a',
        hideBarsDelay: 500,
        download: false
    });
</script>
<? else: ?>
<p>Rien a afficher</p>
<? endif; ?>

<p><a href="/galerie-des-membres" class="btn btn-primary">Tous les membres</a></p>
