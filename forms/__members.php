<?php

include('cpce/libcpce.php');

function print_list($group_id, $exclude_groups = array(1), $show_func = FALSE) {
?>
<table class="list">
    <tr>
        <th>Nom</th>
        <th>Pseudo</th>
        <? if ($show_func) : ?><th>Fonction</th><? endif; ?>
        <th>Ancienneté</th>
        <th>Site web</th>
    </tr>

<?php
    global $season_end;
    $db = &JFactory::getDBO();
    $table = $db->getPrefix().'comprofiler';
    $tjoomla = $db->getPrefix().'users';
    $tablegp = $db->getPrefix().'user_usergroup_map';
    $exclude_list = join(', ', $exclude_groups);
    $qry = "SELECT firstname, lastname, username, cb_fonction1, cb_membredepuis, website "
        ."FROM `$tjoomla` AS u, `$table` AS p "
        ."WHERE u.id = p.user_id AND u.block = 0 "
            ."AND u.id IN (SELECT user_id FROM $tablegp WHERE group_id = $group_id) "
            ."AND u.id NOT IN (SELECT user_id FROM $tablegp WHERE group_id IN ($exclude_list)) "
        ."ORDER BY cb_membredepuis ASC";
    echo "<!-- $qry -->";

    $db->setQuery($qry);
    $results = $db->loadAssocList();

    foreach ($results as $member):
        $member_for = $season_end - date('Y', strtotime($member['cb_membredepuis']));
        $simple_url = str_replace('http://', '', str_replace('https://', '', $member['website']));
        $url = ((substr($member['website'], 0, 4) == 'http') ? '':'http://').$member['website'];
    ?>
    <tr>
        <td><a href="/profil/<?= $member['username'] ?>"><?= format_name($member['firstname'].' '.$member['lastname']); ?></a></td>
        <td><?= $member['username']; ?></td>
        <? if ($show_func) : ?><td><?= format_name(str_replace('|*|', ', ', $member['cb_fonction1'])); ?></td><? endif; ?>
        <td><?= $member_for ?></td>
        <td><a href="<?= $url ?>"><?= $simple_url ?></a></td>
    </tr>
    <? endforeach; ?>
</table>

<?
}
?>


<h1>Liste des membres</h1>

<h2>Bureau</h2>
<? print_list(10, array(18), TRUE); ?>

<h2>Membres d'honneur</h2>
<? print_list(15); ?>

<h2>Membres</h2>
<? print_list(9, array(10, 18)); ?>
