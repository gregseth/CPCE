<?

include('cpce/libcpce.php');

function random_pic($dir)
{
    $files = glob($dir . '/*.[jJ][pP][gG]');
    if (empty($files)) {
        return "";
    } else {
        $file = array_rand($files);
        return $files[$file];
    }
}
function exclude($needle) {
    return substr($needle, 0, 1) != '.' && $needle != 'default';
}

$BASE_DIR = 'cpce/upload/default';

// array_values reindexes keys (array_filter preserves them)
$user_list = array_values(array_filter(scandir($BASE_DIR), 'exclude'));
$cover_list = array();
foreach ($user_list as $k => $user) {
    $picture = random_pic($BASE_DIR.'/'.$user);
    if (empty($picture)) {
        unset($user_list[$k]);
    }
    $cover_list[] = realpath($picture);
}

// DEBUG:
//print_r($user_list);
//print_r($cover_list);
?>

<h2>Galerie des membres</h2>

<div id="gallery-container">
<ul id="gallery">
<? foreach ($user_list as $i => $user): ?>
    <?
    $fullname = format_name($user);
    $first = explode(' ', $fullname)[0];
    $last = explode(' ', $fullname)[1];
    ?>
    <li><a href="galerie-des-membres?event=submit&user=<?= urlencode($user) ?>">
        <p class="image"><img src="cpce/resize.php?size=300&img=<?= urlencode($cover_list[$i]) ?>" /></p>
        <p class="name"><?= $first ?> <?= substr($last, 0, 1) ?>.</p>
    </a></li>
<? endforeach; ?>
</ul>
</div>
