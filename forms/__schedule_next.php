<?php
setlocale(LC_ALL, "fr_FR.UTF8");
date_default_timezone_set('Europe/Paris');

$db = & JFactory::getDBO();
$tb = 'icagenda_events';
$table = $db->getPrefix().$tb;

$today = date('Y-m-d');
// if we're after september 1st, $year is current year, last year instead
$year = intval(date('Y')) - ((time() > strtotime(date('Y').'-09-01')) ? 0 : 1);

$season_start = $year.'-09-01';
$season_end = ($year+1).'-07-01';

$qry = "SELECT title, startdate, enddate, dates, displaytime, shortdesc, `desc` "
      ."FROM $table "
      ."WHERE startdate > '$season_start' AND startdate < '$season_end' AND state = 1 "
      ."ORDER BY startdate ASC";

$db->setQuery($qry);
$results = $db->loadAssocList();

$event_list = array();
foreach ($results as $event) {
  array_push($event_list, $event);
  if ($event['dates'] != 'a:1:{i:0;s:16:"0000-00-00 00:00";}') {
    preg_match_all('/"(\d\d\d\d-\d\d-\d\d) \d\d:\d\d"/', $event['dates'], $dates);
    foreach ($dates[1] as $date) {
      array_push($event_list, array(
        'title' => $event['title'],
        'startdate' => $date.' '.explode(' ', $event['startdate'])[1],
        'enddate' => $date.' '.explode(' ', $event['enddate'])[1],
        'displaytime' => $event['displaytime'],
        'shortdesc' => $event['shortdesc'],
        'desc' => $event['desc'],
      ));
    }
  }
}

$events_date = array_column($event_list, 'startdate');
array_multisort($events_date, SORT_ASC, $event_list);
?>

<?php
foreach ($event_list as $event):
  $timestamp = strtotime($event['enddate']);
  if (date("YmdHM", $timestamp) > date('YmdHM')):
    echo "<!-- ".date("YmdHM", $timestamp)." > ".date('YmdHM')." -->";
    $title = $event['title'];
    $is_today = date("Y-m-d", $timestamp) == $today;
    // linkify plain text urls
    $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
    $short_desc = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $event['shortdesc']);
?>
<div id="whatsnext">
<h2>Prochain rendez-vous&nbsp;: <strong><?= ($is_today) ? "aujourd'hui" : strftime("%A %e %B", $timestamp) ?></strong></h2>
<div class="desc">
<h3 class="date">«&nbsp;<?= $title ?>&nbsp;»</h3>
<?= !empty($short_desc) ? '<p class="summary">'.$short_desc.'</p>':''; ?>
</div>
<p class="details"><a href="/programme">Voir tous les détails et le reste du programme…</a></p>
</div>
<?php
    break;
  endif;
endforeach;
?>
