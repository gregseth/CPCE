function confirm_delete(gear, form) {
    if (form.value == 'delete') {
        return window.confirm("Êtes-vous sûr de vouloir supprimer le matériel suivant ?\n\n"+gear);
    }
    return true;
}

function toggle_borrow(gear_id, borrower_id) {
    (function($) {
        $('#borrow_'+gear_id).prop('disabled', borrower_id == 0);
    })(jQuery);
}