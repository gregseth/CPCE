(function($) {
    $.fn.extend({
        toggleText: function(a, b){
            return this.text(this.text() == b ? a : b);
        }
    });
    $(document).ready(function() {
        $('#toggle-past-events').click(function() {
            $(this).toggleText('Afficher les événements passés', 'Masquer les événements passés');
            $('.past').toggle();
        });
        $('#toggle-ics-link').click(function() {
            $('#ics-link').toggle(500);
        });
        $('#ics-link').click(function() {
            $(this).select();
        });
    });
})(jQuery);