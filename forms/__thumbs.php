<?

$max_images = (!empty($_GET['limit'])) ? $_GET['limit'] : 3;
$username = &JFactory::getUser()->name;
if (empty($username)) {
    header('Location: /');
}

if (empty($_SESSION['reason']) || !empty($_POST['reason'])) {
    $_SESSION['reason'] = (!empty($_POST['reason'])) ? $_POST['reason'] : 'default';
    $_SESSION['fullpath'] = 'cpce/upload/'.$_SESSION['reason'].'/'.$username;
}
if (empty($_SESSION['reason']) || !empty($_GET['reason'])) {
    $_SESSION['reason'] = (!empty($_GET['reason'])) ? $_GET['reason'] : 'default';
    $_SESSION['fullpath'] ='cpce/upload/'.$_SESSION['reason'].'/'.$username;
}
if ($_SESSION['reason'] == 'default') {
    $_SESSION['title'] = 'Galerie personnelle';
    $max_images = 20;
} else {
    $_SESSION['title'] = end(explode(' - ', $_SESSION['reason']));
    // checking if max number of uploads is specified
    preg_match('/ \((\d+)\)$/', $_SESSION['reason'], $nb_img_array);
    if (!empty($nb_img_array)) {
        // removing number in title
        $_SESSION['title'] = str_replace($nb_img_array[0], '', $_SESSION['title']); // index 0 is the full match
        // setting the limit
        $max_images = $nb_img_array[1];                                             // index 1 the first matching group
    }
}

echo '<!-- ';
print_r($_SESSION);
echo ' -->';


$dir = $_SESSION['fullpath'];
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}

$thumb_files = glob($dir.'/*.[jJ][pP][gG]');
$nbpics = count($thumb_files);
$remaining_uploads = $max_images - $nbpics;
?>

<h1>Téléchargement des images «&nbsp;<?= $_SESSION['title'] ?>&nbsp;»</h1>

<? if ($nbpics > 0): ?>
<ul class="gallery">
    <? foreach ($thumb_files as $file): ?>
    <li>
        <figure>
            <img src="<?= 'cpce/resize.php?size=400&img='.urlencode(realpath($file)) ?>" alt="Image Thumb">
            <figcaption><?= basename($file) ?> <a href="?option=com_chronoforms&chronoform=__delete&remove=<?= urlencode(realpath($file)) ?>">Supprimer</a></figcaption>
        </figure>
    </li>

    <? endforeach; ?>
</ul>
<? else: ?>
<p>Aucune image à afficher</p>
<? endif; ?>

<? if ($_SESSION['reason'] == 'default'): ?>
<p><a href="?option=com_chronoforms&chronoform=__gallery&event=submit&user=<?= $username ?>" class="btn btn-primary">Galerie publique</a></p>
<? else: ?>
<p>N'attendez pas le dernier moment, dans l'après midi de vendredi il sera déjà trop tard…<p>
<? endif; ?>

<? if ($nbpics < $max_images): ?>
    <p>Vous pouvez ajouter <?= $remaining_uploads ?> image<?= ($remaining_uploads > 1) ? 's' : '' ?></p>
    <p><a href="?option=com_chronoforms&chronoform=__upload"><input class="btn btn-primary" value="Ajouter des images" type="button" /></p>
<? endif; ?>
