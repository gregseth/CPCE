<?
require_once('cpce/read_exif.php');
require_once('cpce/libcpce.php');

$voter_id = JFactory::getUser()->id;
// 10 is the group id of the "bureau" group
// $is_admin = in_array(10, JFactory::getUser()->getAuthorisedGroups());
// authorized users:
// - 173: greg
// - 52:  loic
// - 250: gwen 
// - 288: jean-marie
$is_admin = in_array($voter_id, array(58, 173, 250, 288));
// is the polling timeframe is open
$lockfile = 'cpce/vote_is_open_contest';
$is_vote_open = file_exists($lockfile);
$lockresults = 'cpce/hold_results';
$hold_results = file_exists($lockresults);

if ($is_admin && !empty($_GET['action'])) {
    if (!$is_vote_open && $_GET['action'] == 'open') {
        touch($lockfile);
        touch($lockresults);
        $is_vote_open = true;
    }

    if ($is_vote_open && $_GET['action'] == 'close') {
        unlink($lockfile);
        $is_vote_open = false;
    }

    if ($_GET['action'] == 'results') {
        unlink($lockresults);
        header('Location: /resultats-concours');
    }
}

$now = new DateTime('now', new DateTimeZone('Europe/Paris'));

$available_paths = glob('cpce/upload/*');
$is_contest = FALSE;
if (!empty($available_paths)) {
    $i = 0;
    do {
        $path = $available_paths[$i];
        $i++;
    } while (strpos($path, '- Concours -') === false);
    /* REMOVE FOR PROD ***************************************************** */
    //$path = 'cpce/upload_archives/2024.03.01 - Portrait';
    /* REMOVE FOR PROD ***************************************************** */

    $date_dot = explode(' - ', basename($path))[0];
    $is_contest = file_exists($path.'/list.txt'); //(strpos(strtolower($path), '- concours -') === false) ? false : true;
    if ($is_contest) {
        // read the ordered list of files and remove empty lines
        $picture_list = array();
        foreach (array_filter(file($path.'/list.txt', FILE_IGNORE_NEW_LINES)) as $item) {
            $picture_list[] = $path.'/'.$item;
        }
    } else {
        $picture_list = glob($path . '/*/*.[jJ][pP][gG]');
    }
    if ($is_vote_open) {
        if (empty($_SESSION['marks_multi'])) {
            $_SESSION['marks_multi'] = array_fill(0, sizeof($picture_list), array('tech'=>-1,'arts'=>-1,'feel'=>-1));
        }
        $sql = "SELECT picture_id, cat, mark FROM contest_marks_multi WHERE voter_id=$voter_id AND date='$date_dot'";
        $db = JFactory::getDBO();
        $db->setQuery($sql);
        foreach ($db->loadAssocList() as $previous_marks) {
            $_SESSION['marks_multi'][$previous_marks['picture_id']][$previous_marks['cat']] = $previous_marks['mark'];
        }
    } else {
        unset($_SESSION['marks_multi']);
    }
}

$choices = array();
foreach (range(0, 20) as $i) {
    $choices[$i] = $i;
}
$item_file = $path."/items.txt";
if (file_exists($item_file)) {
    $choices = array_merge(array(0), file($item_file, FILE_IGNORE_NEW_LINES));
    unset($choices[0]);
}

$categories = [
    'tech' => 'Technique',
    'arts' => 'Créativité',
    'feel' => 'Ressenti'
];

?>

<h1>Notation</h1>
<? if ($is_vote_open): ?>
<h2><?= array_pop(explode(' - ', $path)) ?></h2>

<? if (empty($picture_list)): ?>
<p>Aucune image disponible pour aujourd'hui</p>
<? else: ?>
<ol id="contest">
<? foreach ($picture_list as $pic_id => $pic): ?>
    <?
    $pic_path = urlencode(realpath($pic));
    $pic_info = explode('/', $pic);
    $author = $pic_info[3];
    $title = $pic_info[4];
    ?>
    <li id="item_<?= $pic_id ?>">
        <a href="<?= $pic ?>" target="_blank" data-sub-html=".caption">
            <img src="cpce/resize.php?size=300&img=<?= $pic_path ?>" title="<?= $is_admin ? $author : '' ?>" alt="<?= $title ?>" />
            <? if (!$is_contest): ?>
            <div class="caption">
                <p class="title"><?= format_name($author)." — ".$title ?></p>
                <p class="exif"><?= exif(realpath($pic)) ?></p>
            </div>
            <? endif; ?>
        </a>
        <? foreach ($categories as $cat => $label): ?>
        <div class="mark">
            <label><?= $label ?></label>
            <select name="pic_id_<?= $pic_id ?>_<?= $cat ?>" id="pic_id_<?= $pic_id ?>_<?= $cat ?>" onchange="setMark(<?= $pic_id ?>, '<?= $cat ?>', this.value)">
                <option value="-1">—</option>
                <? foreach ($choices as $k => $v): ?>
                <option value="<?= $k ?>" <?= ($_SESSION['marks_multi'][$pic_id][$cat] == $k) ? 'selected' : '' ?>><?= $v ?></option>
                <? endforeach; ?>
            </select>
        </div>
        <? endforeach; ?>
    </li>
<? endforeach; ?>
</ol>
<button class="btn btn-primary" id="submit" onclick="saveMarks('<?= $date_dot ?>')">Envoyer</button>
<script type="text/javascript">
    lightGallery(document.getElementById('contest'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: 'li a',
        hideBarsDelay: 1000,
        download: false
    });
</script>
<? endif; ?>

<? if ($is_admin): ?>
<div id="adminbox">
    <p>Ne cliquer sur le bouton « clôturer le vote » qu'une fois que tout le monde a fini de voter afin de pouvoir accéder aux résutats définitifs.</p>
    <p><a href="/vote-concours?action=close" class="btn btn-danger">Clôturer le vote</a></p>
</div>
<? endif; ?>
<? else: ?>
<p>Les votes ne sont pas ouverts pour le moment…</p>

<? if ($is_admin): ?>
<p><a href="/vote-concours?action=open" class="btn btn-success">Ouvrir le vote</a></p>
<? if ($hold_results): ?>
<p><a href="/vote-concours?action=results" class="btn btn-warning">Publier les résultats</a></p>
<? endif; ?>
<? endif; ?>
<? endif; ?>
