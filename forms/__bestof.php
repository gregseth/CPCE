<?

$voter_id = JFactory::getUser()->id;
$now = new DateTime('now', new DateTimeZone('Europe/Paris'));
//dbg:$now->setDate(2019, 5, 31); //:end
$local_hour = $now->format('H');
$date_dot = $now->format('Y.m.d');

// Reading list of directories
$categories = array_filter(file('cpce/bestof_2018-2019.txt', FILE_IGNORE_NEW_LINES));
$cat_id = 0;
if (!empty($_GET['cat'])) {
    $cat_id = $_GET['cat'];
}
$cat = $categories[$cat_id];
$_SESSION['category'] = $cat;

$path = 'cpce/upload_archives/'.$cat;
$picture_list = glob($path . '/*/*.[jJ][pP][gG]');

if (empty($_SESSION['marks'][$cat])) {
    $_SESSION['marks'][$cat] = array_fill(0, sizeof($picture_list), 0);
}
$db = JFactory::getDBO();
$sql_cat = $db->quote($cat);
$sql = "SELECT picture_id, mark FROM bestof_marks WHERE voter_id=$voter_id AND year=2018 AND theme=$sql_cat";
$db->setQuery($sql);
foreach ($db->loadAssocList() as $previous_marks) {
    $_SESSION['marks'][$cat][$previous_marks['picture_id']] = $previous_marks['mark'];
}


?>

<h1>Notation florilège</h1>

<form action="vote-bestof" method="GET">
<select name="cat" onchange="this.form.submit()">
<? for ($i=0; $i<count($categories); $i++): ?>
    <option value="<?= $i ?>" <?= ($cat_id == $i) ? 'selected' : '' ?>><?= array_pop(explode('/', $categories[$i])) ?></option>
<? endfor; ?>
</select>
</form>

<h2><?= end(explode(' - ', $path)) ?></h2>

<? if (empty($picture_list)): ?>
<p>Aucune image disponible pour aujourd'hui</p>
<? else: ?>
<ol id="contest">
<? foreach ($picture_list as $pic_id => $pic): ?>
    <?
    $pic_path = urldecode(realpath($pic));
    $pic_info = explode('/', $pic);
    $author = $pic_info[3];
    $title = $pic_info[4];
    ?>
    <li id="item_<?= $pic_id ?>">
        <a href="<?= $pic ?>" target="_blank"><img src="cpce/resize.php?size=300&img=<?= $pic_path ?>" title="<?= $author ?>" alt="<?= $title ?>" /></a>
        <select name="pic_id_<?= $pic_id ?>" onchange="setMark(<?= $pic_id ?>, this.value)">
            <option value="0">—</option>
            <? for ($i = 6; $i <= 20; $i++): ?>
            <option value="<?= $i ?>" <?= ($_SESSION['marks'][$cat][$pic_id] == $i) ? 'selected' : '' ?>><?= $i ?></option>
            <? endfor; ?>
        </select>
    </li>
<? endforeach; ?>
</ol>
<button class="btn btn-primary" id="submit" onclick="saveMarks()">Envoyer</button>
<? endif; ?>
