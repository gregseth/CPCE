function setMark(id, cat, value) {
    (function($) {
        $("#item_"+id)
            .removeClass('submit-fail')
            .removeClass('sumbit-ok')
            .removeClass('submit-missing');

        if (value > -1) {
            // https://photo-club-ermont.fr/?option=com_chronoforms&chronoform=__ranking_multi&event=submit&id=1&cat=tech&mark=8
            $.get('/', {
                'option': 'com_chronoforms',
                'chronoform': '__ranking_multi',
                'event': 'submit',
                'id': id,
                'cat': cat,
                'mark': value
            }).done(function(data) {
                // assertig error if any data printed
                if (data) alert(data);
                $("#item_"+id).addClass('submit-ok', 500);
                setTimeout(() => {
                    $("#item_"+id).removeClass('submit-ok', 500);
                }, 1000);
            }).fail(function() {
                alert('Erreur de communication avec le serveur.\nEssayez de recharger la page.');
                $("#item_"+id).addClass('submit-fail');
                $("#pic_id_"+id+"_"+cat).value = -1;
            });
        }

    })(jQuery);
}
 
function saveMarks(event_date) {
    (function($) {
        missing_marks = [];
        $('[name^=pic_id_]').each(function (id) {
            if (this.value == -1) {
                pic_id = Math.floor(id/3);
                pic_number = pic_id+1;
                if (missing_marks.indexOf(pic_number) === -1) {
                    missing_marks.push(pic_number);
                }
                $("#item_"+pic_id).addClass('submit-missing');
            }
        });

        if (missing_marks.length == 0) {
            $.get('/', {
                'option': 'com_chronoforms',
                'chronoform': '__ranking_multi',
                'event': 'submit',
                'save': 'true',
                'date': event_date
            }).done(function(data) {
                // assertig error if any data printed
                if (data) alert(data);
                $('#submit')
                    .removeClass('btn-danger')
                    .addClass('btn-success')
                    .text('✔︎ Vote enregistré');
            }).fail(function() {
                alert('Erreur de communication avec le serveur.\nRechargez la page et réessayez.');
                $('#submit')
                    .addClass('btn-danger')
                    .text('✘ Erreur');
            });
        } else {
            if (missing_marks.length == 1) {
                alert('La note de l\'image '+missing_marks+' est incomplète');
            } else {
                alert('Les notes des images '+missing_marks.join(', ')+' sont incomplètes');
            }
            $('html, body').animate({
                scrollTop: $("#item_"+missing_marks[0]-1).offset().top - 600
            }, 500);
        }
    })(jQuery);
}