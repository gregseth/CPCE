<?php

include('cpce/libcpce.php');

// loading Community Builder API
include_once( JPATH_ADMINISTRATOR . '/components/com_comprofiler/plugin.foundation.php' );

if ($_POST['user'] > 0) {
    $user_id = $_POST['user'];
    $db = &JFactory::getDBO();
    $prefix = $db->getPrefix();
    $today = date('Y-m-d');
    $user = CBuser::getUserDataInstance( $user_id );

    var_dump($_POST);

    switch ($_POST['action']) {
        case 'activate':
            $user->set('approved', 1);
            $user->set('confirmed', 1);
            $user->set('block', 0);
            $user->set('cb_membredepuis', date('Y-m-d'));

            if ( $user->store() ) {
                activateUser( $user, 1, 'UserRegistration' );
            }
            break;
        case 'disable':
            // standard unsubscription procedure is to set block flag to 1,
            // remove user from all groups and add it to the « non réinscrits »
            // group (group_id = 13)
            $user->set('block', 1);
            $user->set('cb_anneedesinscription', $season_start-1);
            $db->setQuery("DELETE FROM ${prefix}user_usergroup_map WHERE user_id=$user_id");
            $db->execute();
            $db->setQuery("INSERT INTO ${prefix}user_usergroup_map (user_id, group_id) VALUES ('$user_id', '13')");
            $db->execute();
            break;
        case 'payment':
            $user->set('cb_lastcotiz', $season_start);
            $user->set('cb_flagcotiz', 1);
            $user->set('cb_cotiz', $_POST['fee']);
            break;
        case 'fpf':
            $user->set('cb_adherentfpf', $season_start);
            break;
        default:
            break;
    }

    if ( $user->store() ) {
        if ($_POST['action'] == 'activate') {
            activateUser( $user, 1, 'UserRegistration' );
        }
    } else {
        echo "An error occured while processing the action.";
    }
}

header('Location: /gestion-inscriptions');

?>
