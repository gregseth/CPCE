<?

// only allow logged in users
if (JFactory::getUser()->id == 0) {
    header('Location: /');
}


$db = & JFactory::getDBO();
$table = $db->getPrefix().'users';
$tablecb = $db->getPrefix().'comprofiler';
$tablegp = $db->getPrefix().'user_usergroup_map';

$qry = "SELECT firstname, lastname, username, cb_selfie "
      ."FROM `$table` AS u, `$tablecb` AS p "
      ."WHERE u.id = p.id AND u.block = 0 "
      // group 18 is the "special" group for test members that should not appear here
        ."AND u.id NOT IN (SELECT user_id FROM $tablegp WHERE group_id = 18) "
      ."ORDER BY firstname ASC";

$db->setQuery($qry);
$results = $db->loadAssocList();

function format_name($name) {
    return ucwords(strtolower($name), " -'");
}


?>
<h1>Trombinoscope</h1>
<ul id="trombi">
<? foreach($results as $user): ?>
    <li>
        <img src="/images/<?=empty($user['cb_selfie']) ? 'empty_trombi.png' : 'comprofiler/'.$user['cb_selfie'] ?>" />
        <p class="name"><?= format_name($user['firstname']) ?><br><?= format_name($user['lastname'])?></p>
        <p class="pseudo"><?= $user['username'] ?></p>
    </li>
<? endforeach; ?>
</ul>
</body>
</html>
