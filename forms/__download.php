<?php

$username = &JFactory::getUser()->name;
if (empty($username)) {
  header('Location: /');
}

$gallery_name = $_POST['reason'];
if (empty($gallery_name)) {
  $gallery_name = $_GET['reason'];
}
if (empty($username)) {
  header('Location: /?option=com_chronoforms&chronoform=__pickdir&action=download');
}
$title = end(explode(' - ', $gallery_name));

// summary infos
$summary = array();
$total_participants = 0;
$total_images = 0;
$total_last_upload = 0;

// Get real path for our folder
chdir('cpce/upload');
//$fulldir = 'cpce/upload/'.$gallery_name;
$names = glob($gallery_name.'/*');
foreach ($names as $name) {
  $images = glob($name.'/*.[jJ][pP][gG]');
  $file_count = count($images);
  if ($file_count > 0) {
    $last_upload = 0;
    foreach ($images as $i) {
      $last_upload = max($last_upload, filemtime($i));
    }
    $total_last_upload = max($total_last_upload, $last_upload);
    $summary[basename($name)] = array('count' => $file_count, 'ts' => $last_upload, 'images' => $images);
    $total_participants++;
    $total_images += $file_count;
  }
}

chdir('../..');
// archive creation
$zip_name = 'cpce/'.$gallery_name.'.zip';
$is_contest = (strpos(strtolower($gallery_name), '- concours -') === false) ? false : true;

// if the archive is out of date (i.e. some pictures have been uploaded since last zip creation)
if (file_exists($zip_name) && $total_last_upload > filemtime($zip_name)) {
  unlink($zip_name);
}

if (!file_exists($zip_name)) {
  $start = microtime(true);
  $zip = new ZipArchive();
  $zip->open($zip_name, ZIPARCHIVE::CREATE);

  chdir('cpce/upload');
  foreach ($names as $name) {
    $images = glob($name.'/*.[jJ][pP][gG]');
    foreach ($images as $i) {
      if ($is_contest) {
        $zip->addFile($i);
      } else {
        $zip->addFile($i, basename($name).' - '.basename($i));
      }
    }
  }

  $zip->close();
  chdir('../..');
}
$zip_date = filemtime($zip_name);
?>
<h1>État des téléchargements des images «&nbsp;<?= $title ?>&nbsp;»</h1>

<table id="summary">
  <tr>
    <th>Nom</th>
    <th>Nombre d'images</th>
    <th>Date modification</th>
    <th>Images</th>
  </trh>
<? foreach ($summary as $name => $values): ?>
  <tr>
    <td><?= ucfirst($name) ?></td>
    <td><?= $values['count'] ?></td>
    <td><?= date('d/m/Y H:i:s', $values['ts']) ?></td>
    <td>
      <? foreach ($values['images'] as $i): ?>
        <a href="cpce/upload/<?= $i ?>">
        <img src="https://photo-club-ermont.fr/cpce/resize.php?size=120&img=%2Fhome%2Fphotocluz%2Fwww%2Fcpce%2Fupload%2F<?= $i ?>">
        </a>
        <? endforeach; ?>
  </tr>
<? endforeach; ?>
</table>

<p>Date de la dernière mise jour&nbsp;: <?= date('d/m/Y H:i:s', $total_last_upload) ?></p>
<p>Fichier ZIP généré le&nbsp;: <?= date('d/m/Y H:i:s', $zip_date) ?></p>
<p>Nombre de participants&nbsp;: <?= $total_participants; ?></p>
<p>Nombre total d'images&nbsp;: <?= $total_images; ?></p>
<p><a href="<?= $zip_name ?>" class="btn btn-primary">Télécharger .zip</a></p>
