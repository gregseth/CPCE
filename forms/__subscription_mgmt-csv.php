<?php

include('cpce/libcpce.php');

$db = &JFactory::getDBO();

$table = $db->getPrefix().'comprofiler';
$tjoomla = $db->getPrefix().'users';
$tablegp = $db->getPrefix().'user_usergroup_map';

$qry = "SELECT u.id, firstname, lastname, city, cb_lastcotiz, cb_adherentFPF, email, phone, zipcode, cb_address, cb_datedenaissance, cb_membredepuis "
      ."FROM `$tjoomla` AS u, `$table` AS p "
      ."WHERE u.id = p.user_id AND u.block = 0 "
      // group 18 is the "special" group for test members that should not appear here
        ."AND u.id NOT IN (SELECT user_id FROM $tablegp WHERE group_id = 18) "
      ."ORDER BY lastname ASC";

$db->setQuery($qry);
$results = $db->loadAssocList();


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: inline');
//header('Content-Disposition: attachment; filename="membres-cpce.csv"');
echo "Nom;Prénom;Téléphone;e-mail;Cotisation;FPF;Adresse;CP;Ville;Date de naissance;Date d'inscription".PHP_EOL;

foreach ($results as $member) {

    $has_paid = $member['cb_lastcotiz'] == $season_start;
    $has_paid_fpf = $member['cb_adherentFPF'] == $season_start;

    $line = array(
        format_name($member['lastname']),
        format_name($member['firstname']),
        format_phone($member['phone']),
        strtolower($member['email']),
        $has_paid ? 'oui':'non',
        $has_paid_fpf ? 'oui':'non',
        '"'.strtolower($member['cb_address']).'"',
        $member['zipcode'],
        format_name($member['city']),
        $member['cb_datedenaissance'],
        $member['cb_membredepuis']
    );
    echo join(';', $line).PHP_EOL;
}

exit();
?>