<?
require_once('cpce/read_exif.php');
require_once('cpce/libcpce.php');

$lockfile = 'cpce/vote_is_open_contest';
$is_vote_open = file_exists($lockfile);
$lockresults = 'cpce/hold_results';
$hold_results = file_exists($lockresults);
$is_admin = JFactory::getUser()->id == 173;

const TECH = 2;
const ARTS = 0;
const FEEL = 1;

$sort = "total DESC, picture_id ASC";
$sort_link = array(
    'id' => '?sort=id',
    'avg' => '?sort=avg',
    'std' => '?sort=std',
    'min' => '?sort=min',
    'max' => '?sort=max'
);
$sort_class = array(
    'id' => '',
    'avg' => 'sorted',
    'std' => '',
    'min' => '',
    'max' => ''
);
$custom_sort = "";
$colspan_offset = 1;
$sort_where = "";
if (!empty($_GET['sort'])) {
    switch ($_GET['sort']) {
        case 'id':      $sort = "picture_id ASC";                   break;
        case 'avg':     $sort = "average DESC, picture_id ASC";     break;
        case 'std':     $sort = "stddev DESC";                      break;
        case 'min':     $sort = "min ASC";                          break;
        case 'max':     $sort = "max DESC";                         break;
        case 'tech':
        case 'arts':
        case 'feel':
            $sort_where = " AND cat = '".$_GET['sort']."'"; 
            break;
    }
    if (in_array($_GET['sort'], ['tech', 'arts', 'feel'])) {
        $custom_sort = "custom-sort";
        $colspan_offset--;
    }
    /*
    if ($_GET['sort'] != 'avg') {
        $custom_sort = "custom-sort";
        $sort_class['avg'] = '';
        $colspan_offset = 0;
    }
    $sort_link[$_GET['sort']] = '';
    $sort_class[$_GET['sort']] = 'sorted';
    */
}

function get_author($path) {
    $split_path = explode('/', $path);
    return $split_path[array_key_last($split_path)-1];
}

function get_initials($name) {
    $initials = "";
    foreach (explode(' ', str_replace('-', ' ', $name)) as $element) {
        $initials .= substr($element, 0, 1);
    }
    return strtoupper($initials);
}

$date_index = ((!$is_vote_open && !$hold_results) || $is_admin) ? 0 : 1;
$show_voter_name = $is_admin;

$db = JFactory::getDBO();
$sql_dates = "SELECT DATE_FORMAT(date, '%Y.%m.%d') FROM contest_marks_multi GROUP BY date ORDER BY date DESC";

$db->setQuery($sql_dates);
$date_list = array_slice($db->loadColumn(), $date_index);

$date_dot = $date_list[0];
if (!empty($_GET['date'])) {
    $date_dot = $_GET['date'];
}
$display_path = "";
$select_data = array();
foreach ($date_list as $date) {
    $path = glob('cpce/upload/'.$date.'*')[0];
    if (empty($path)) {
        $path = glob('cpce/upload_archives/'.$date.'*')[0];
        if (empty($path)) {
            $path = glob('cpce/upload/.'.$date.'*')[0];
        }
    } else {
        if ($skip_first_in_upload && !$first_in_upload_skipped) {
            $first_in_upload_skipped = true;
            continue;
        }
    }

    $select_data[$date] = array_pop(explode('/', $path));

    if ($date == $date_dot) {
        $is_contest = (strpos(strtolower($path), '- concours -') === false) ? false : true;
        if ($is_contest) {
            $picture_list = array();
            foreach (array_filter(file($path.'/list.txt', FILE_IGNORE_NEW_LINES)) as $item) {
                $picture_list[] = $path.'/'.$item;
            }
        } else {
            $picture_list = glob($path . '/*/*.[jJ][pP][gG]');
        }
        $display_path = $path;$is_contest = true;
    }
}

$sql = "SELECT picture_id, SUM(mark) AS total, ROUND(AVG(mark),2) AS avg FROM contest_marks_multi "
    ."WHERE date='$date_dot' ".$sort_where
    ."GROUP BY picture_id "
    ."ORDER BY $sort";
$db->setQuery($sql);
$results = $db->loadAssocList();
$rank = 1;

$nb_pic = count($results);
$author_list = array();
foreach($picture_list as $pic) {
    $author_list[] = explode('/', $pic)[3];
}
$author_list = array_unique($author_list);
$nb_authors = count($author_list);
$db->setQuery("SELECT DISTINCT voter_id FROM contest_marks_multi WHERE date='$date_dot' ORDER BY voter_id");
$voters = $db->loadColumn();
$nb_voters = count($voters);

$previous_total = 0;
$exaequo_count = 0;

$summary = array(
    'picture_id' => array(),
    'rank' => array(),
    'voter_id' => array(),
    'marks' => array(
        'tech' => [],
        'arts' => [],
        'feel' => []
    )
);
/*dbg:
echo '<pre>';
print_r($results);
echo '</pre>';
*/
?>

<form action="vote-resultats" method="GET">
<select name="date" onchange="this.form.submit()">
<? foreach ($select_data as $date => $title): ?>
    <option value="<?= $date ?>" <?= ($date == $date_dot) ? 'selected' : '' ?>><?= $title ?></option>
<? endforeach; ?>
</select>
</form>

<dl>
    <dt>Nombre de photos :</dt>
    <dd><?= $nb_pic ?></dd>
    <dt>Nombre de participants :</dt>
    <dd><?= $nb_authors ?></dd>
    <dt>Nombre de votants :</dt>
    <dd><?= $nb_voters ?></dd>
</dl>

<? if (!$is_contest): ?>
<div id="imglist">
<h2>Les trois favorites</h2>
<ul>
<? 
for ($i=0; $i<3; $i++): 
    $path = $picture_list[$results[$i]['picture_id']];
?>
    <li class="image favitem">
        <a href="<?= $picture_list[$results[$i]['picture_id']] ?>" data-sub-html=".caption">
            <img src="cpce/resize.php?size=400&img=<?= urlencode(realpath($path)) ?>" />
            <div class="caption">
                <p class="title"><?= get_author($path)." — ".get_title($path) ?></p>
                <p class="exif"><?= exif(realpath($path)) ?></p>
            </div>
        </a>
    </li>
<? endfor; ?> 
</ul>
<h2>Toutes les images</h2>
<ul>
<? foreach ($picture_list as $pic): ?>
    <li class="image listitem">
        <a href="<?= $pic ?>" data-sub-html=".caption">
            <img src="cpce/resize.php?size=120&img=<?= urlencode(realpath($pic)) ?>" />
            <div class="caption">
                <p class="title"><?= get_author($pic)." — ".get_title($pic) ?></p>
                <p class="exif"><?= exif(realpath($pic)) ?></p>
            </div>
        </a>
    </li>
<? endforeach; ?>
</ul>
</div>
<script type="text/javascript">
    lightGallery(document.getElementById('imglist'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: '.image a',
        hideBarsDelay: 1000,
        download: false
    });
</script>
<? endif; ?>

<? if ($is_contest or $is_admin): ?>
<p class="sort">
    Classement :
    <button class="btn btn-<?= ($_GET['sort']=='')?'info':'primary' ?>" onclick="sort('')">Général</button>
    <button class="btn tech header btn-<?= ($_GET['sort']=='tech')?'info':'primary' ?>" onclick="sort('tech')">Technique</button>
    <button class="btn arts header btn-<?= ($_GET['sort']=='arts')?'info':'primary' ?>" onclick="sort('arts')">Créativité</button>
    <button class="btn feel header btn-<?= ($_GET['sort']=='feel')?'info':'primary' ?>" onclick="sort('feel')">Ressenti</button>
</p>
<p class="commands">
    <button class="btn btn-primary" onclick="toggle('mark')">Notes individuelles</button>
    <!--button class="btn btn-primary" onclick="toggle('stats')">Statistiques</button-->
</p>

<div id="container">
<table>
    <tr>
        <th class=""></th>
        <th colspan="2">Image</th>
        <th>Auteur</th>
        <th class="<?= $custom_sort ?>">Note /20</th>
        <!--th colspan="2">Total </th-->
        <th>Moy</th>
        <th class="stats">Min</th>
        <th class="stats">Max</th>
        <th class="stats"><abbr title="Écart type">σ</abbr></th>
        <th class="mark" colspan="<?= count($voters) ?>">Notes</th>
    </tr>
<? if ($show_voter_name): ?>
    <tr>
        <td colspan="<?= 5+$colspan_offset ?>"></td>
        <td colspan="3" class="stats"></td>
    <? foreach ($voters as $vid): ?>
        <? $summary['voter_id'][] = $vid; ?>
        <td class="mark voter<?=$vid?>" onclick="highlight(<?=$vid?>)"><abbr title="<?= ($vid < 1000000000) ? format_name(JFactory::getUser($vid)->name) : $vid ?>"><?= ($vid < 1000000000) ? get_initials(JFactory::getUser($vid)->name) : $vid ?></abbr></td>
    <? endforeach; ?>
    </tr>
<? else: ?>
    <? foreach ($voters as $vid): ?>
        <? $summary['voter_id'][] = $vid; ?>
    <? endforeach; ?>
<? endif; ?>
<? foreach ($results as $pic): ?>
<?
if ($pic['total'] < $previous_total) {
    $rank += $exaequo_count + 1;
    $exaequo_count = 0;
} elseif ($previous_total > 0) {
    $exaequo_count++;
}
$previous_total = $pic['total'];
$id = $pic['picture_id'];
$num = $id+1;
$summary['picture_id'][] = $num;
$summary['rank'][] = $rank;

$sql_mark_summary = "SELECT SUM(mark) as total, ROUND(AVG(mark),1) as avg, MIN(mark) as min, MAX(mark) as max, ROUND(STD(mark),1) as stddev, cat, voter_id FROM contest_marks_multi WHERE date='$date_dot' AND picture_id=$id GROUP BY cat ORDER BY cat";
$db->setQuery($sql_mark_summary);
$results_mark_summary = $db->loadAssocList();
$sql_marks = "SELECT mark, cat, voter_id FROM contest_marks_multi WHERE date='$date_dot' AND picture_id=$id ORDER BY voter_id, cat";
$db->setQuery($sql_marks);
$results_marks = $db->loadAssocList();
/*dbg:
echo '<pre>';
print_r($results_marks);
echo '</pre>';
*/
$split_path = explode('/', $picture_list[$id]);

$author = $split_path[3];
$auth_std = strtolower(str_replace(' ', '-', $author));
//dbg: echo $author;
?>
    <tr class="table-result-row <?= $auth_std ?>">
        <td class=""><?= $rank ?></td>
        <td>n°<?= $num ?></td>
        <td class="image">
            <a href="<?= $picture_list[$id] ?>" data-sub-html=".caption">
                <img src="cpce/resize.php?size=120&img=<?= urlencode(realpath($picture_list[$id])) ?>" />
                <div class="caption">
                    <p class="title"><?= "#$rank (n°$num) — $author — ".get_title($picture_list[$id]) ?></p>
                    <p class="exif"><?= exif(realpath($picture_list[$id])) ?></p>
                </div>
            </a>
        </td>
        <td><a href="javascript:filterAuthor('<?= $auth_std ?>')"><?= format_name($author) ?></td>
        <td class="<?= $custom_sort ?>"><?= $pic['avg'] /*str_replace('.0', '', str_replace('.3', '<sup>⅓</sup>', str_replace('.7', '<sup>⅔</sup>', $pic['avg'])))*/ ?></td>
        <!--td><?= $pic['total'] ?></td>
        <td>
            <span class="tech header <?= $_GET['sort']=='tech' ? 'on' : 'off' ?>"><?= $results_mark_summary[TECH]['total'] ?></span><br />
            <span class="arts header <?= $_GET['sort']=='arts' ? 'on' : 'off' ?>"><?= $results_mark_summary[ARTS]['total'] ?></span><br />
            <span class="feel header <?= $_GET['sort']=='feel' ? 'on' : 'off' ?>"><?= $results_mark_summary[FEEL]['total'] ?></span>
        </td-->
        <td>
            <span class="tech header <?= $_GET['sort']=='tech' ? 'on' : 'off' ?>"><?= $results_mark_summary[TECH]['avg'] ?></span><br />
            <span class="arts header <?= $_GET['sort']=='arts' ? 'on' : 'off' ?>"><?= $results_mark_summary[ARTS]['avg'] ?></span><br />
            <span class="feel header <?= $_GET['sort']=='feel' ? 'on' : 'off' ?>"><?= $results_mark_summary[FEEL]['avg'] ?></span>
        </td>
        <td class="stats">
            <span class="tech <?= $_GET['sort']=='tech' ? 'on' : 'off' ?>"><?= $results_mark_summary[TECH]['min'] ?></span><br />
            <span class="arts <?= $_GET['sort']=='arts' ? 'on' : 'off' ?>"><?= $results_mark_summary[ARTS]['min'] ?></span><br />
            <span class="feel <?= $_GET['sort']=='feel' ? 'on' : 'off' ?>"><?= $results_mark_summary[FEEL]['min'] ?></span>
        </td>
        <td class="stats">
            <span class="tech <?= $_GET['sort']=='tech' ? 'on' : 'off' ?>"><?= $results_mark_summary[TECH]['max'] ?></span><br />
            <span class="arts <?= $_GET['sort']=='arts' ? 'on' : 'off' ?>"><?= $results_mark_summary[ARTS]['max'] ?></span><br />
            <span class="feel <?= $_GET['sort']=='feel' ? 'on' : 'off' ?>"><?= $results_mark_summary[FEEL]['max'] ?></span>
        </td>
        <td class="stats">
            <span class="tech <?= $_GET['sort']=='tech' ? 'on' : 'off' ?>"><?= $results_mark_summary[TECH]['stddev'] ?></span><br />
            <span class="arts <?= $_GET['sort']=='arts' ? 'on' : 'off' ?>"><?= $results_mark_summary[ARTS]['stddev'] ?></span><br />
            <span class="feel <?= $_GET['sort']=='feel' ? 'on' : 'off' ?>"><?= $results_mark_summary[FEEL]['stddev'] ?></span>
        </td>
    <? 
    $marks_for_pic = array(
        'tech' => [],
        'arts' => [],
        'feel' => []
    );
    for ($i = 0; $i < count($results_marks); $i+=3):
        $voter_name = '';
        $vid = $results_marks[$i]['voter_id'];
        if ($show_voter_name && $vid < 1000000000) {  // filtering out fake users of manual notation
            $voter_name = format_name(JFactory::getUser($vid)->name);
        }
        $marks_for_pic['total'][] = $results_marks[$i+TECH]['mark']+$results_marks[$i+ARTS]['mark']+$results_marks[$i+FEEL]['mark'];
        $marks_for_pic['tech'][] = $results_marks[$i+TECH]['mark'];
        $marks_for_pic['arts'][] = $results_marks[$i+ARTS]['mark'];
        $marks_for_pic['feel'][] = $results_marks[$i+FEEL]['mark'];
    ?>
        <td class="mark voter<?=$vid?>">
            <span class="<?= $voter_name ?> tech <?= $_GET['sort']=='tech' ? 'on' : 'off' ?>"><?= $results_marks[$i+TECH]['mark'] ?></span><br />
            <span class="<?= $voter_name ?> arts <?= $_GET['sort']=='arts' ? 'on' : 'off' ?>"><?= $results_marks[$i+ARTS]['mark'] ?></span><br />
            <span class="<?= $voter_name ?> feel <?= $_GET['sort']=='feel' ? 'on' : 'off' ?>"><?= $results_marks[$i+FEEL]['mark'] ?></span>
        </td>
    <? 
    endfor;
    $summary['marks']['total'][] = $marks_for_pic['total'];
    $summary['marks']['tech'][] = $marks_for_pic['tech'];
    $summary['marks']['arts'][] = $marks_for_pic['arts'];
    $summary['marks']['feel'][] = $marks_for_pic['feel'];
    ?>
    </tr>
<? endforeach; ?>
    <tr id="last" style="display:none">
        <td colspan="10" class="image">
            <a href="images/black.png"><img src="images/black.png" /></a>
        </td>
    </tr>
</table>
</div>

<!-- <pre><? // print_r(json_encode($summary, JSON_NUMERIC_CHECK)); ?></pre> -->
<!--
<?php
/*
echo "summary = [\n";
for ($i=0; $i<count($summary['marks']); $i++) {
    for ($j=0; $j<count($summary['marks'][$i]); $j++) {
        echo "{ p: ".$summary['picture_id'][$i].", r: ".$summary['rank'][$i].", v: ".$summary['voter_id'][$j].", m: ".$summary['marks'][$i][$j]."},\n";
    }
}
echo "];\n";
*/

$marks = $summary['marks'];
?>
-->
<?/*
echo "<pre>";
$marks = $summary['marks'];
print_r($summary['marks']);
echo "</pre><p>tech</p><pre>";
print_r($marks['tech']);
echo "</pre><p>arts</p><pre>";
print_r($marks['arts']);
echo "</pre><p>feel</p><pre>";
print_r($marks['feel']);
echo "</pre>";*/
?>
<div class="stats">
<?
$titles = [
    'total' => 'Note finale (/60)',
    'tech'  => 'Technique',
    'arts'  => 'Créativité',
    'feel'  => 'Ressenti'
];
foreach (array('total', 'tech', 'arts', 'feel') as $cat):
    $stats_path = $display_path.'/stats_'.$cat.'_'.count($summary['voter_id']).'.png';
    $summary['marks'] = $marks[$cat];
    $summary['range'] = [ 'min' => 0, 'max' => ($cat == 'total') ? 60 : 20 ];
    $payload = json_encode($summary, JSON_NUMERIC_CHECK);

    if (!file_exists($stats_path)) {
        # deleting all previous files, if any
        foreach(glob($display_path.'/stats_'.$cat.'*.png') as $f) {
            unlink($f);
        }
        $ch = curl_init('https://photo.gregseth.net/cpce/plot');
        $out = fopen($stats_path, 'wb');

        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FILE, $out); 

        // curl -o stats.png -X POST -d $payload --header "Content-Type: application/json" https://photo.gregseth.net/cpce/plot
        curl_exec($ch);
        curl_close($ch);
        fclose($out);
    }
    ?>
    <h3>Statistiques des notes&nbsp;: <?= $titles[$cat] ?></h3>
    <!--<pre><? print_r($payload) ?></pre>-->
    <img src="<?= $stats_path ?>" /><br />
<? endforeach;?>
</div>
<script type="text/javascript">
    lightGallery(document.getElementById('container'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: '.image a',
        hideBarsDelay: 1000,
        download: false
    });
</script>
<? endif; ?>

