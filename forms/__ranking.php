<?
require_once('cpce/read_exif.php');
require_once('cpce/libcpce.php');

$voter_id = JFactory::getUser()->id;
$voter_name = JFactory::getUser()->name;
$voter_pics = array();
// 10 is the group id of the "bureau" group
// $is_admin = in_array(10, JFactory::getUser()->getAuthorisedGroups());
// authorized users:
// - 173: greg
// - 52:  loic
// - 250: gwen 
// - 288: jean-marie
$is_admin = in_array($voter_id, array(58, 173, 250, 288));
// is the polling timeframe is open
$lockfile = 'cpce/vote_is_open';
$is_vote_open = file_exists($lockfile);
$lockresults = 'cpce/hold_results';
$hold_results = file_exists($lockresults);

if ($is_admin && !empty($_GET['action'])) {
    if (!$is_vote_open && $_GET['action'] == 'open') {
        touch($lockfile);
        touch($lockresults);
        $is_vote_open = true;
    }

    if ($is_vote_open && $_GET['action'] == 'close') {
        unlink($lockfile);
        $is_vote_open = false;
    }

    if ($_GET['action'] == 'results') {
        unlink($lockresults);
        header('Location: /projections');
    }
}

$available_paths = glob('cpce/upload/*');
$is_contest = FALSE;
if (!empty($available_paths)) {
    $i = 0;
    do {
        $path = $available_paths[$i];
        $i++;
    } while (strpos($path, '- Expo -') !== false);

    $date_dot = explode(' - ', basename($path))[0];
    $is_contest = file_exists($path.'/list.txt'); //(strpos(strtolower($path), '- concours -') === false) ? false : true;
    if ($is_contest) {
        // read the ordered list of files and remove empty lines
        $picture_list = array();
        foreach (array_filter(file($path.'/list.txt', FILE_IGNORE_NEW_LINES)) as $item) {
            $picture_list[] = $path.'/'.$item;
        }
    } else {
        $picture_list = glob($path . '/*/*.[jJ][pP][gG]');
    }
    if ($is_vote_open) {
        if (empty($_SESSION['marks'])) {
            $_SESSION['marks'] = array_fill(0, sizeof($picture_list), 0);
        }
        $sql = "SELECT picture_id, mark FROM contest_marks WHERE voter_id=$voter_id AND date='$date_dot'";
        $db = JFactory::getDBO();
        $db->setQuery($sql);
        foreach ($db->loadAssocList() as $previous_marks) {
            $_SESSION['marks'][$previous_marks['picture_id']] = $previous_marks['mark'];
        }
    } else {
        unset($_SESSION['marks']);
    }
}

$tz = new DateTimeZone('Europe/Paris');
$now = new DateTime('now', $tz);
$date = DateTime::createFromFormat('Y.m.d', $date_dot, $tz);
// negative if in the past, -0 the day before, +0 the day of the vote, positive after
$days_countdown = $date->diff($now)->format('%R%a'); 
$allow_vote = $days_countdown > 0 || ($days_countdown == '+0' && $now->format('H') >= 19);

?>

<h1>Notation</h1>
<? if ($is_vote_open): ?>
<h2><?= array_pop(explode(' - ', $path)) ?></h2>

<? if (empty($picture_list)): ?>
<p>Aucune image disponible pour aujourd'hui</p>
<? else: ?>
<ol id="contest">
<? foreach ($picture_list as $pic_id => $pic): ?>
    <?
    $pic_path = urlencode(realpath($pic));
    $pic_info = explode('/', $pic);
    $author = $pic_info[3];
    if ($voter_name == $author) {
        $voter_pics[] = $pic_id;
    }
    $title = $pic_info[4];
    ?>
    <li id="item_<?= $pic_id ?>">
        <a href="<?= $pic ?>" target="_blank" data-sub-html=".caption">
            <img src="cpce/resize.php?size=300&img=<?= $pic_path ?>" title="<?= $is_admin ? $author : '' ?>" alt="<?= $title ?>" />
            <? if (!$is_contest): ?>
            <div class="caption">
                <p class="title"><?= format_name($author)." — ".$title ?></p>
                <p class="exif"><?= exif(realpath($pic)) ?></p>
            </div>
            <? endif; ?>
        </a>
    </li>
<? endforeach; ?>
</ol>
<? if ($allow_vote): ?>
<h2>Notes</h2>
<p>Renseignez le numéro de la photo à laquelle vous voulez donner trois, deux et une étoile (vous ne pouvez pas voter pour vos photos).</p>
<form name="vote" action="/vote?event=submit" method="post" onsubmit="return validateMarks()">
    <label for="three">⭐️⭐️⭐️</label>
    <select name="three" id="three">
        <option value="-1" selected>—</option>
        <? foreach (range(0, count($picture_list)) as $i): ?>
        <option value="<?= $i ?>" <?= (in_array($i, $voter_pics) ? 'disabled' : '') ?> <?= ($_SESSION['marks'][$i] == 20) ? 'selected' : '' ?>><?= $i+1 ?></option>
        <? endforeach; ?>
    </select>
    <br>
    <label for="two">⭐️⭐️</label>
    <select name="two" id="two">
        <option value="-1" selected>—</option>
        <? foreach (range(0, count($picture_list)) as $i): ?>
        <option value="<?= $i ?>" <?= (in_array($i, $voter_pics) ? 'disabled' : '') ?> <?= ($_SESSION['marks'][$i] == 15) ? 'selected' : '' ?>><?= $i+1 ?></option>
        <? endforeach; ?>
    </select>
    <br>
    <label for="one">⭐️</label>
    <select name="one" id="one">
        <option value="-1" selected>—</option>
        <? foreach (range(0, count($picture_list)) as $i): ?>
        <option value="<?= $i ?>" <?= (in_array($i, $voter_pics) ? 'disabled' : '') ?> <?= ($_SESSION['marks'][$i] == 10) ? 'selected' : '' ?>><?= $i+1 ?></option>
        <? endforeach; ?>
    </select>
    <br>
    <input type="hidden" name="date" value="<?= $date_dot ?>">
    <input type="submit" class="btn btn-primary" id="submit">
</form>
<? endif; ?>
<script type="text/javascript">
    lightGallery(document.getElementById('contest'), {
        zoom: true,
        fullScreen: true,
        subHtmlSelectorRelative: true,
        selector: 'li a',
        hideBarsDelay: 1000,
        download: false
    });
</script>
<? endif; ?>

<? if ($is_admin): ?>
<div id="adminbox">
    <p>Ne cliquer sur le bouton « clôturer le vote » qu'une fois que tout le monde a fini de voter afin de pouvoir accéder aux résutats définitifs.</p>
    <p><a href="/vote?action=close" class="btn btn-danger">Clôturer le vote</a></p>
</div>
<? endif; ?>
<? else: ?>
<p>Les votes ne sont pas ouverts pour le moment…</p>

<? if ($is_admin): ?>
<p><a href="/vote?action=open" class="btn btn-success">Ouvrir le vote</a></p>
<? if ($hold_results): ?>
<p><a href="/vote?action=results" class="btn btn-warning">Publier les résultats</a></p>
<? endif; ?>
<? endif; ?>
<? endif; ?>
