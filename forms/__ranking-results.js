function toggle(className) {
    (function($) {
        $('.'+className).toggle();
    })(jQuery);
}
function filterAuthor(author) {
    (function($) {
        $('.table-result-row').not('.'+author).toggle();
    })(jQuery);
}
function highlight(voterid) {
    (function($) {
        wasSelected = $('.voter'+voterid).first().hasClass('highlight');
        $('.mark').removeClass('highlight');
        if (!wasSelected) {
            $('.voter'+voterid).addClass('highlight');
        }
    })(jQuery);
}