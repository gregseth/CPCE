<?php
setlocale(LC_ALL, "fr_FR");
date_default_timezone_set('Europe/Paris');

$db = & JFactory::getDBO();
$tb = 'icagenda_events';
$table = $db->getPrefix().$tb;

$date = time();
$date_start = date('Y').'-07-01';
// if we're before july 1st, $year is last year, current year instead
$year = intval(date('Y')) - (($date < strtotime($date_start)) ? 1 : 0);
$next_year = $year+1;

$date_start = $year.'-09-01';
$date_end = ($year+1).'-07-01';

$qry = "SELECT GREATEST(MAX(created), MAX(modified)) FROM $table";
$db->setQuery($qry);
$result = $db->loadResult();
$last_modified = gmdate('Ymd\THis\Z', strtotime($result));

$qry = "SELECT title, startdate, enddate, dates, shortdesc, `desc`, id, place, displaytime "
      ."FROM $table "
      ."WHERE startdate > '$date_start' AND startdate < '$date_end' AND state = 1 "
      ."ORDER BY startdate ASC";
$db->setQuery($qry);
$results = $db->loadAssocList();

function sanitize($str) {
    $breaks = array("<br />","<br>","<br/>");
    return strip_tags(str_ireplace($breaks, "\n", str_replace('&#39;', "'", html_entity_decode($str))));
}

header('Content-Type: text/calendar; charset=utf-8');
header('Content-Disposition: inline; filename="programme-cpce.ics"');
header('Cache-Control: no-cache, no-store, must-revalidate');
header('Pragma: no-cache');
header('Expires: Mon, 17 Oct 1983 10:30:00 GMT');

$ical = "BEGIN:VCALENDAR\r\n"
  ."PRODID:-//CPCE/NONSGML CPCEcal V1.0//FR\r\n"
  ."VERSION:2.0\r\n"
  ."URL:https://photo-club-ermont.fr/programme\r\n"
  ."NAME:Programme CPCE {$year}/{$next_year}\r\n"
  ."LAST-MODIFIED:{$last_modified}\r\n"
  ."X-WR-CALNAME:Programme CPCE {$year}/{$next_year}\r\n"
  ."BEGIN:VTIMEZONE\r\n"
    ."TZID:Europe/Paris\r\n"
    ."X-LIC-LOCATION:Europe/Paris\r\n"
    ."BEGIN:DAYLIGHT\r\n"
    ."TZOFFSETFROM:+0100\r\n"
    ."TZOFFSETTO:+0200\r\n"
    ."TZNAME:CEST\r\n"
    ."DTSTART:19700329T020000\r\n"
    ."RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=3\r\n"
    ."END:DAYLIGHT\r\n"
    ."BEGIN:STANDARD\r\n"
    ."TZOFFSETFROM:+0200\r\n"
    ."TZOFFSETTO:+0100\r\n"
    ."TZNAME:CET\r\n"
    ."DTSTART:19701025T030000\r\n"
    ."RRULE:FREQ=YEARLY;INTERVAL=1;BYDAY=-1SU;BYMONTH=10\r\n"
    ."END:STANDARD\r\n"
  ."END:VTIMEZONE\r\n"
  ."TIMEZONE-ID:Europe/Paris\r\n"
  ."X-WR-TIMEZONE:Europe/Paris\r\n"
  ."REFRESH-INTERVAL;VALUE=DURATION:PT1H\r\n"
  ."X-PUBLISHED-TTL:PT1H\r\n";

foreach ($results as $event):
  $name = $event['title'];

  $timestamp_start = strtotime($event['startdate']);
  $timestamp_end =  strtotime($event['enddate']);

  $date_id = gmdate('Ymd\THis\Z', $timestamp_start);
  $date_start = date('Ymd', $timestamp_start);
  $time_start = date('His', $timestamp_start);
  $date_end = date('Ymd', $timestamp_end);
  $time_end = date('His', $timestamp_end);

  $short_comment = $event['shortdesc'];
  $long_comment = $event['desc'];
  $ical_desc = preg_replace("/(\r?\n){2,}/", "\\n", sanitize($short_comment."\\n".$long_comment));
  $ical_uid = "{$date_id}-{$event['id']}@photo-club-ermont.fr";
  $location = sanitize($event['place']);

  $event_time = "DTSTART;TZID=Europe/Paris:{$date_start}T{$time_start}\r\n"
              ."DTEND;TZID=Europe/Paris:{$date_end}T{$time_end}\r\n";
  if ($event['displaytime'] == 0) {
    $date_end = date('Ymd', $timestamp_end+24*60*60);
    $event_time = "DTSTART;VALUE=DATE:{$date_start}\r\n"
                . "DTEND;VALUE=DATE:{$date_end}\r\n";
  }

  $ical .= "BEGIN:VEVENT\r\n"
    ."UID:{$ical_uid}\r\n"
    ."DTSTAMP:{$date_id}\r\n"
    ."SUMMARY:{$name}\r\n"
    .$event_time
    ."DESCRIPTION:{$ical_desc}\r\n"
    ."LOCATION:{$location}\r\n"
    ."END:VEVENT\r\n";

  // handling the case of multiple events
  if ($event['dates'] != 'a:1:{i:0;s:16:"0000-00-00 00:00";}') {
    preg_match_all('/"(\d\d\d\d-\d\d-\d\d) \d\d:\d\d"/', $event['dates'], $dates);
    $n = 1;
    foreach ($dates[1] as $date) {
      $date = str_replace('-', '', $date);
      $date_id = gmdate('Ymd\THis\Z', strtotime($date));
      $ical_uid = "{$date_id}-{$event['id']}-{$n}@photo-club-ermont.fr";
      $event_time = "DTSTART;TZID=Europe/Paris:{$date}T{$time_start}\r\n"
                  . "DTEND;TZID=Europe/Paris:{$date}T{$time_end}\r\n";

      $ical .= "BEGIN:VEVENT\r\n"
        ."UID:{$ical_uid}\r\n"
        ."DTSTAMP:{$date_id}\r\n"
        ."SUMMARY:{$name}\r\n"
        .$event_time
        ."DESCRIPTION:{$ical_desc}\r\n"
        ."LOCATION:{$location}\r\n"
        ."END:VEVENT\r\n";
      $n++;
    }
  }
endforeach;

$ical .= "END:VCALENDAR\r\n";
print($ical);
exit();
?>