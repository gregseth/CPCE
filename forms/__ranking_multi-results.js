function toggle(className) {
    (function($) {
        $('.'+className).toggle();
    })(jQuery);
}
function filterAuthor(author) {
    (function($) {
        $('.table-result-row').not('.'+author).toggle();
    })(jQuery);
}
function highlight(voterid) {
    (function($) {
        wasSelected = $('.voter'+voterid).first().hasClass('highlight');
        $('.mark').removeClass('highlight');
        if (!wasSelected) {
            $('.voter'+voterid).addClass('highlight');
        }
    })(jQuery);
}
function sort(sort_by) {
    uri = window.location.href;
    key = 'sort';
    value = sort_by;
    
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        window.location.href = uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        window.location.href = uri + separator + key + "=" + value;
    }
}