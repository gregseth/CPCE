<?

/** Requires the following GET vars:
 *   img:   The url of the picture
 *   size:  The longest side of the picture
 */


$img = $_GET['img'];
$size = empty($_GET['size']) ? 200 : $_GET['size'];
$debug = !empty($_GET['debug']);

$type = substr(strrchr($img,'.'), 1);
$ttype = str_replace('jpg','jpeg',strtolower($type));
$imgcr = 'imagecreatefrom'.$ttype;
$imgecho = 'image'.$ttype;
$ok = FALSE;

// disabling cache
$cached_thumb = $img.'.'.$size.'.thumb';

if ($debug) {
    ini_set('display_errors',1);
    error_reporting(E_ALL);
    echo "<pre>";
}

if (!file_exists($cached_thumb)) {
    if ($debug) {
        echo "source path: $img\n"
            ."source path stripped: ".stripslashes($img)."\n"
            ."file exists: ".(file_exists(stripslashes($img))?"yes":"no")."\n";
    }

    $src = null;
    $dest = null;
    if (file_exists(stripslashes($img)) && filesize(stripslashes($img))<8*1024*1024) {
        $src = $imgcr(stripslashes($img));
    }
    if ($src) {
        $sx = imagesx($src);
        $sy = imagesy($src);

        if (max($sx, $sy) <= $size) {
            $dx = $sx;
            $dy = $sy;
        } else {
            $r=$sx/$sy;
            $dx = round($size * (($sx > $sy) ? 1 : $r));
            $dy = round($size * (($sx > $sy) ? 1/$r : 1));
        }
        if ($debug) echo "resized to: $dy x $dy\n";
        $dest = imagecreatetruecolor($dx, $dy);
        if ($debug) echo "thumb creation: ".($dest ? "SUCCESS" : "FAILURE")."\n";
    }

    if ($dest) {
        imagecopyresampled($dest, $src, 0, 0, 0, 0, $dx, $dy, $sx, $sy);

        $ok = $imgecho($dest, $cached_thumb, 100);
        imagedestroy($dest);
    }
} else {
    $ok = TRUE;
}

$redir_url = "/images/404_thumb.jpg";
if ($ok) {
    $redir_url = str_replace('home/photocluz/www/', '', $cached_thumb);
} else {
    // remove if anything went wrong
    if (file_exists($cached_thumb)) {
        if ($debug) {
            echo "something went wrong, deleting cached image: ".$cached_thumb;
        }
        unlink($cached_thumb);
    }
}
if ($debug) {
    echo "redirection: Location: ".$redir_url;
    echo "</pre>";
    echo "<img src=\"$redir_url\" />";
} else {
    header("Content-type: image/jpg");
    header("Location: ".$redir_url);
}
exit;
?>
