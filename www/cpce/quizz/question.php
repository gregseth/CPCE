<html lang="fr">
<?php
session_start();

function get_correct_answer($q) {
    $letter = 'A';
    foreach ($q->answers as $ans) {
        if ($ans->correct) {
            return $letter;
        } else {
            $letter++;
        }
    }
}

if (empty($_SESSION['questions']))
    header('Location: index.php');

$qid = isset($_GET['q']) ? $_GET['q'] : rand(0, count($_SESSION['questions'])-1)+1;
$q = $_SESSION['questions'][$qid-1];
shuffle($q->answers);

$current_question = array(
    'id' => $qid,
    'text' => $q->question,
    'answer' => get_correct_answer($q),
    'isopen' => false
);
file_put_contents('current_question.json',json_encode($current_question));
?>
<head>
<meta charset="UTF-8">
    <title>Grand quizz CPCE</title>
<link rel="shortcut icon" href="/images/logo/icon.png">
<style type="text/css">
@font-face {
    font-family: 'Conduit ITC';
    src: url('Conduit_ITC.ttf');
}
@font-face {
    font-family: 'Copperplate';
    src: url('Copperplate.ttf');
}
body {
    overflow: hidden;
    width: 1920px;
    height: 1080px;
    margin: 0;
    padding: 0;
    background: #000 url('images/background.png') no-repeat;
    font-family: 'Conduit ITC', 'Helvetica Neue', sans-serif;
    font-size: 32px; /* 28 */
    font-weight: 500;
    color: #FFF;
}
header, #image, #question, .answer, #results {
    position: absolute;
}
header {
    top: 10px;
    left: 10px;
    z-index: 2;
}
footer {
    position: absolute;
    bottom: 0;
    right: 0;
    margin: .5em;
    text-align: right;
    font-size: .5em;
    color: rgba(255, 255, 255, .3);
}
div {
    display: flex;
    align-items: center;
}
#image {
    width: 100%;
    height: 630px;
    text-align: center;
    justify-content: center;
}
#image img {
    border: solid #FFF 5px;
    box-shadow: 0 0 45px #FFF, 0 0 20px #FFF;
    max-height: 500px;
}
#question {
    top: 632px;
    left: 152px;
    width: 1616px;
    height: 132px;
    margin: 0;
    text-align: center;
    justify-content: center;
    font-size: 56px; /* 52 */
    line-height: 1em;
}
.answer {
    width: 788px;
    height: 74px;
    margin: 0;
    padding: 0 30px 0 90px;
    line-height: 1.8ex;
}
.answer.valid {
    background-position: 0 2px;
    background-repeat: no-repeat;
}
#A {
    top: 828px;
    left: 46px;
}
#A:hover.valid {
    background: url('images/valid-answer-a.png');
}
#B {
    top: 828px;
    left: 966px;
}
#B:hover.valid {
    background: url('images/valid-answer-b.png');
}
#C {
    top: 930px;
    left: 46px;
}
#C:hover.valid {
    background: url('images/valid-answer-c.png');
}
#D {
    top: 930px;
    left: 966px;
}
#D:hover.valid {
    background: url('images/valid-answer-d.png');
}
#results {
    display: none;
    top: 22px;
    right: 22px;
    width: 384px;
    height: 478px;
    margin: 0;
    padding: 43px 42px 63px 42px;
    background: #000 url('images/poll-chart.png');
    box-shadow: 0 0 20px #000;
}
#results div.graphbar {
    display: inline-block;
    align-self: flex-end;
    width: 82px;
    height: 0;
    margin: 0 7px;
    background: linear-gradient(0deg, rgba(180,0,255,.9) 0%, rgba(23,239,255,.9) 100%);
    text-align: center;
    font-size: .8em;
}
#countdown {
    display: none;
    position: absolute;
    top: 260px;
    left: 120px;
    font-family: 'Copperplate', sans-serif;
    font-size: 120px;
    color: #F7931E;
}
#countdown::before {
    content: ':';
}
</style>
<script src="/js/jquery.min.js"></script>
<script>
let cntdn = 45;

function setOpen(yesno = true) {
    $.get('qopenclose.php', { setopen: yesno }).fail(function() {
        alert('reset failed');
    });
}
function countdown() {
    $('#countdown').show();
    $('#countdown').text(cntdn.toString().padStart(2, '0'));
    cntdn--;
    if (cntdn >= -1) {
        setTimeout(countdown, 1000);
    } else {
        // reset user poll remote
        setOpen(false);
        setTimeout(showResults, 2000);
        $('#countdown').hide();
    }
}
function showResults() {
    $('#results').css('display', 'flex').hide().fadeIn('slow');
    $.getJSON('results.php', {'q': <?= $qid ?>}).done(function(results) {
        let total = 0;
        for (r of results) {
            total += parseInt(r.num, 10);
        }
        for (r of results) {
            let percent = Math.round(r.num/total*100)+'%';
            $('#result'+r['answer']).text(r.num).animate({
                height: percent
            }, 1500);
        }
    }).fail(function() {
        alert('Error while getting results.');
    });
}
$(document).ready(function() {
    $('#image img').click(function() {
        setOpen();
        countdown();
    });
});
</script>
</head>
<body>
    <!-- question id: <?= $qid ?> -->
    <!-- question index: <?= $_SESSION['qindex'] ?> -->
    <!-- question list: <? print_r($_SESSION['qlist']); ?> -->
    <?php
    $link = ($_SESSION['qindex'] == count($_SESSION['qlist']))
        ? 'index.php'
        : 'question.php?q='.($_SESSION['qlist'][$_SESSION['qindex']++]+1);
    ?>
    <header><a href="<?= $link ?>"><img src="images/logo.png"></a></header>
    <section>
    <div id="countdown">XX</div>
    <div id="image"><img src="images/q/<?= $q->image ?>"></div>
    <div id="question"><?= /*$qid.'. '.*/$q->question ?></div>
    <? $qletter = 'A'; ?>
    <? foreach ($q->answers as $a): ?>
    <div id="<?= $qletter++ ?>" class="answer <?= $a->correct ? 'valid':'' ?>"><?= $a->text ?></div>
    <? endforeach; ?>
    </section>
    <section id="results">
        <div id="resultA" class="graphbar"></div>
        <div id="resultB" class="graphbar"></div>
        <div id="resultC" class="graphbar"></div>
        <div id="resultD" class="graphbar"></div>
    </section>
    <footer>Q#<?= str_pad($qid, 3, '0', STR_PAD_LEFT) ?></footer>
</body>
</html>
