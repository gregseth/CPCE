<?php
session_start();
// init current question to empty
file_put_contents('current_question.json',json_encode(array(
    'id' => null,
    'text' => '',
    'answer' => '',
    'isopen' => false
)));
// false returns json as objects instead of dict
$_SESSION['questions'] = json_decode(file_get_contents('questions.json'), false, 512, JSON_UNESCAPED_UNICODE);
$_SESSION['qlist'] = array_rand($_SESSION['questions'], 25);
shuffle($_SESSION['qlist']);
$_SESSION['qindex'] = 0;

//var_dump($questions);
?>
<html lang="fr">
<head>
<meta charset="UTF-8">
    <title>Grand quizz CPCE</title>
<link rel="shortcut icon" href="/images/logo/icon.png">
<style type="text/css">
body {
    background: #000 url('images/introbg.jpg');
}
</style>
</head>
<body>
<a id="next" href="question.php?q=<?= $_SESSION['qlist'][$_SESSION['qindex']++]+1 ?>"><img src="images/logo-big.png"></a>
</body>
</html>