#!/bin/bash

for i in {0..50}
do
  curl -sL "https://www.diyphotography.net/sony-becomes-the-exclusive-camera-brand-of-the-canadian-press/?playAgain=$i" | sed -r 's/>/>\n/g' | grep -A1 'vq.+label|wpvq-question-img' | grep '/div|/label|<img' | grep -v '^</div>$' | sed -r -e 's_^(.+)</div>$_\n\1_' -e 's_^(.+)</label>$_\1_' -e 's/^.+data-ezsrc=(.+)>$/\1/' >> quizz.txt
done
