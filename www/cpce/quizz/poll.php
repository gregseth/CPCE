<?php
require_once '../joomla.inc.php';

$voter_id = &JFactory::getUser()->id;
if ($voter_id == 0) {
    header('Location: https://photo-club-ermont.fr/component/comprofiler/login');
}
?>
<html lang="fr">
<head>
<meta charset="UTF-8">
    <title>Quizz</title>
<link rel="shortcut icon" href="/images/logo/icon.png">
<style>
@font-face {
    font-family: 'Conduit ITC';
    src: url('Conduit_ITC.ttf');
}
body {
    margin: 1em;
    background: #000 url('images/blurrybg.jpg') right;
    background-size: auto 100vh;
    font-family: 'Conduit ITC', 'Helvetica Neue', sans-serif;
    font-size: 24pt;
    font-weight: bold;
    color: #FFF;
    text-align: center;
}
button {
    display: block;
    width: calc(100% - 2em);
    margin: 1em;
    padding: 1ex;
    border: .25ex outset rgb(5, 116, 167);
    border-radius: 1em;
    background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(5,116,167,1) 50%, rgba(1,36,115,1) 100%);

    font-family: 'Conduit ITC', 'Helvetica Neue', sans-serif;
    font-size: 2em;
    font-weight: bold;
    color: #FFF;
}
button:active, button:enabled:not(.checked):hover {
    border-style: inset;
    background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(5,116,167,1) 70%, rgba(1,36,115,1) 100%);

}
.valid {
    border: .25ex outset rgba(5,167,30,1);
    background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(5,167,30,1) 50%, rgba(1,115,6,1) 100%);
}
.checked {
    border: .25ex outset rgba(251,200,108,1);
    background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(251,200,108,1) 50%, rgba(203,80,0,1) 100%);
}
button:disabled {
    border: .25ex outset rgba(200,200,200,1);
    background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(200,200,200,1) 50%, rgba(80,80,80,1) 100%);
}
</style>
<script src="/js/jquery.min.js"></script>
<script>
let qinfo = null;
$(document).ready(function() {
    $.ajaxSetup({ cache: false });

    $('button').click(function() {
        $('button').removeClass('valid checked');
        let answer = this.id;

        $.get('save.php', {
            'q': qinfo.id,
            'user': <?= $voter_id ?>,
            'correct': qinfo.answer == answer,
            'ans': answer
        }).done(function(data) {
            // assertig error if any data printed
            if (data) {
                alert(data);
            } else {
                $('#'+answer).addClass('checked');
            }
        }).fail(function() {
            alert('Erreur de communication avec le serveur.\nEssayez de recharger la page.');
            $('#'+answer).removeClass('valid checked');
        });
    });
});

function updateStatus() {
    $.getJSON('/cpce/quizz/current_question.json').done(function(new_qinfo) {
        if (qinfo == null || qinfo.isopen != new_qinfo.isopen || qinfo.id != new_qinfo.id) {
            qinfo = new_qinfo;
            // reset previous answer
            $('button')
                .removeClass('valid checked')
                .prop('disabled', !qinfo.isopen);
            $('#question').text(qinfo.text);
        }
    });
    setTimeout(updateStatus, 2500);
}
updateStatus();
</script>
</head>
<body>
<h1 id="question">?</h1>
<button id="A">A</button>
<button id="B">B</button>
<button id="C">C</button>
<button id="D">D</button>
</body>
</html>