#!/usr/bin/python3
import csv
import json
import copy

Q = {
    'question': '',
    'image': '',
    'answers': [
        { 'text': '', 'correct': True },
        { 'text': '', 'correct': False },
        { 'text': '', 'correct': False },
        { 'text': '', 'correct': False }
    ]
}
QUESTIONS = []

with open('questions.csv', 'r') as csvfile:
    qreader = csv.reader(csvfile, delimiter=';')
    for line in qreader:
        print(line)
        q = copy.deepcopy(Q)
        q['question'] = line[0]
        q['image'] = line[1]
        for i in range(4):
            q['answers'][i]['text'] = line[i+2]
        QUESTIONS.append(q)

with open('questions.json', 'w') as jsonfile:
    json.dump(QUESTIONS, jsonfile, indent=4, ensure_ascii=False)
