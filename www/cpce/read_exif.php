<?

function get_focal($data) {
    return eval('return '.$data['FocalLength'].';').'mm';
}

function get_aperture($data) {
    $frac = explode('/', $data['FNumber']);
    if (!is_null($frac[0]) && !is_null($frac[1])) {
        return 'f/'.(number_format($frac[0])/number_format($frac[1]));
    } else {
        return 'f/?';
    }
}

function get_speed($data) {
    return $data['ExposureTime'].'s';
}

function get_iso($data) {
    return 'ISO '.$data['ISOSpeedRatings'];
}

function get_title($file) {
    return basename($file, '.jpg');
}

function exif($file) {
    $format = '%s + %s // %s – %s – %s – %s';
    $exif = exif_read_data($file);
    return empty($exif['Model']) ? 'Pas de données EXIF' : sprintf( $format,
        $exif['Model'],
        $exif['UndefinedTag:0xA434'],
        get_focal($exif),
        get_speed($exif),
        get_aperture($exif),
        get_iso($exif)
    );
}


// called as a script (otherwise include and use functions directly)
if (basename(__FILE__) == basename($_SERVER["SCRIPT_FILENAME"])) {
    if (!isset($file) && $argc > 1) {
        $file = $argv[1];
        echo exif($file);
    } else {
        exit(1);
    }
}
?>
