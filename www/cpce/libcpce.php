<?
/**
 * Defines variables and functions
 *
 *   - $season_start & $season_end: defines the current season, $season_start is the value of
 *      the year on the first of september $season_end is $season_start+1.
 *   - format_name: returns a clean ucfirst name.
 *   - format_phon: phone number as XX.XX.XX.XX.XX
 */


$date = time();
$date_start = date('Y').'-09-01';
// if we're before september 1st, $season_start is last year, current year instead
global $season_start, $season_end;
$season_start = intval(date('Y')) - (($date < strtotime($date_start)) ? 1 : 0);
$season_end = $season_start+1;



function format_name($name) {
    return ucwords(strtolower($name), " -'");
}

function format_phone($value) {
    $number = str_replace('+33', '0',
        str_replace('.', '',
        str_replace(' ', '',
        $value
        )));
    for ($i=0; $i<4; $i++) {
        $number = substr_replace($number, '.', (4-$i)*2, 0);
    }
    return $number;
}

?>
